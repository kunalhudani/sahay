<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@yield('title') | {{ env('APP_NAME') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        @include('components.client.partials.header_link')
    </head>
    <body data-sidebar="dark">
        <div id="layout-wrapper">
            <x-client-topnavigation />
            <x-client-sidebar />
            <div class="main-content">
                @yield('content')
                @include('components.client.partials.client_modal')
                @include('components.client.partials.footer')
            </div>
        </div>
        <div class="rightbar-overlay"></div>
        @include('components.client.partials.footer_link')
    </body>
</html>
<script type="text/javascript">
    @if(Session::has('messages'))
        $(document).ready(function() {
            @foreach(Session::get('messages') AS $msg) 
                toastr['{{ $msg["type"] }}']('{{$msg["message"]}}');
            @endforeach
        });
    @endif

    @if (count($errors) > 0) 
        $(document).ready(function() {
            @foreach($errors->all() AS $error)
                toastr['error']('{{$error}}');
            @endforeach     
        });
    @endif
</script>