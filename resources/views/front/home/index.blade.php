@extends('layouts.utility')
@section('title','Coming Soon')
@section('content')
<div class="my-5 pt-sm-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <a href="index.html">
                        <img src="{{ env('APP_LOGO') }}" alt="logo" height="24" />
                    </a>
                    <div class="row justify-content-center mt-5">
                        <div class="col-sm-4">
                            <div class="maintenance-img">
                                <img src="{{ asset('images/maintenance.png') }}" alt="" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                    </div>
                    <h4 class="mt-5">Let's get started with Skote</h4>
                    <p class="text-muted">It will be as simple as Occidental in fact it will be Occidental.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection