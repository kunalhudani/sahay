@extends('layouts.admin')
@section('title','Edit Task Status')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Edit Task Status</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.clientDepartmentList') }}">Task Status List</a></li>
                            <li class="breadcrumb-item active">Edit Task Status</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.saveEditedTaskStatus') }}" method="post" id="taskStatusForm" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Task Status<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="task_status" placeholder="Task Status" autocomplete="off" value="{{$find_task_status->task_status}}" required/>
                                <input type="hidden" name="id" value="{{$find_task_status->id}}" id="id">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Update
                                </button>
                                <a href="{{ route('admin.taskStatusList') }}" class="btn btn-secondary waves-effect">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
