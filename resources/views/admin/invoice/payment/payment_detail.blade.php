@extends('layouts.admin')
@section('title','Payment Detail')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Payment Detail</h4>
                    <?php $total_amount = $find_invoice->total_payable - $sum_payment; ?>
                    <h4 class="mb-0 font-size-18">Total Outstading : {{$total_amount}}</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.invoiceList') }}">Invoice List</a></li>
                            <li class="breadcrumb-item active">Payment Detail</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
            <div class="col-12">
                <div class="col-md-5 offset-3" style="margin-bottom: 1%;">
                    <center><!-- <h4 class="mb-0 font-size-18">Enter Letest Payment</h4> -->
                    
                        <button class="btn btn-primary waves-effect waves-light new_payment"  title="Add new Payment" role="button">Enter Letest Payment</button>

                     </center>
                </div>
            </div><br><br>
        </div>     
        <!-- end page title -->
        <!-- end row -->
            <div class="row new_payment_card" style="display: none;">
                <div class="col-lg-6 offset-3">
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.savePaymentDetail') }}" method="post" id="paymentForm" enctype="multipart/form-data">
                            @csrf
                             <div class="form-group">
                                <label>Date of Payment</label>
                                <input type="text" class="form-control" name="date_of_entery" placeholder="dd/mm/yyyy" data-provide="datepicker" autocomplete="off" data-date-autoclose="true" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y") ?>" data-date-end-date="0d">
                                <input type="hidden" name="invoice_id" value="{{$find_invoice->id}}">
                            </div>
                            <div class="form-group">
                                <label>Paid Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="paid_amount" placeholder="Paid Amount" autocomplete="off" value="" required/>
                            </div>

                            <div class="form-group">
                                <label>Tax Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="tax_amount" placeholder="Tax Amount" autocomplete="off" value="" required/>
                            </div>

                            <div class="form-group">
                                <label>TDS Deducted<span class="mandatory">*</span></label>
                                <select class="form-control tds_deducted" name="tds_deducted" required>
                                    <option value="1">No</option>
                                    <option value="2">Yes</option>
                                </select>
                            </div>

                            <div class="form-group tax_taxation" style="display: none;">
                                <label>TDS Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control tax" name="tds_amount" data-msg="Enter TDS Amount" placeholder="TDS Amount" autocomplete="off" value=""/>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-3">
                    @foreach($get_all_payment as $gk => $gv) 
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.saveEditedPaymentDetail') }}" id="saveEditedPaymentDetail" method="post"  enctype="multipart/form-data">
                            @csrf
                            @if($gv->is_invoice_generated == 1)
                                <div class="form-group">
                                    <label>Invoice Amount<span class="mandatory">*</span></label>
                                    <input type="text" class="form-control" name="paid_amount" placeholder="Invoice Amount" autocomplete="off" readonly @if(!is_null($gv['payment_amount'])) value="{{ $gv['payment_amount']['total_payable'] }}" @endif required/>
                                </div>

                            @endif
                             <div class="form-group">
                                <label>Date of Payment</label>
                                <input type="text" class="form-control" name="date_of_entery" placeholder="dd/mm/yyyy"  data-date-autoclose="true" data-provide="datepicker" autocomplete="off" data-msg="Select Date Of Entry" data-date-format="dd/mm/yyyy" value="{{ date('d/m/yy',strtotime($gv->date_of_entery)) }}" data-date-end-date="0d">
                                <input type="hidden" name="invoice_id" value="{{$find_invoice->id}}">
                                <input type="hidden" name="id" value="{{$gv->id}}">
                            </div>

                            <div class="form-group">
                                <label>Paid Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="paid_amount" data-msg="Enter Paid Amount" placeholder="Paid Amount" autocomplete="off" value="{{ $gv->paid_amount }}" required/>
                            </div>

                            <div class="form-group">
                                <label>Tax Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="tax_amount" data-msg="Enter Tax Amount" placeholder="Tax Amount" autocomplete="off" value="{{ $gv->tax_amount }}" required/>
                            </div>

                            <div class="form-group">
                                <label>TDS Deducted<span class="mandatory">*</span></label>
                                <select class="form-control tds_deducted" name="tds_deducted" data-msg="Enter Tax Deducted" required>
                                    <option value="1" @if($gv->tds_deducted == 1) selected @endif>No</option>
                                    <option value="2" @if($gv->tds_deducted == 2) selected @endif>Yes</option>
                                </select>
                            </div>

                            <div class="form-group tax_taxation" @if($gv->tds_deducted == 1) style="display: none;" @endif>
                                <label>TDS Amount<span class="mandatory">*</span></label>
                                <input type="text" class="form-control tax" name="tds_amount" data-msg="Enter TDS Amount" placeholder="TDS Amount" autocomplete="off" value="{{ $gv->tds_amount }}" @if($gv->tds_deducted == 2) required @endif/>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                </button>
                                <a href="{{ route('admin.deletePaymentDetail',['id' => $gv->id, 'invoice_id' => $find_invoice->id]) }}" onclick="return confirm('Linked Genrated Invoices Deleted Are You Sure Want to Delete?');" class="btn btn-secondary waves-effect">
                                    Delete  
                                </a>
                                @if($gv->is_invoice_generated == 0)
                                    <a href="{{ route('admin.generateInvoice',['id' => $gv->id, 'invoice_id' => $find_invoice->id]) }}" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="0">
                                        Generate Invoice
                                    </a>
                                @else
                                    <button class="btn btn-success waves-effect waves-light mr-1 genrate_invoice" name="btn_submit" value="1">
                                    Generate Invoice
                                    </button>
                                @endif
                            </div>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>
$(document).on('click','.genrate_invoice',function(e){
    e.preventDefault();
    var genrate_invoice = $(this).val();
    console.log(genrate_invoice);
    if(genrate_invoice == 1){
        
        toastr.error('Invoice Already Genrated You can Update By Genrated Invoice Listing');
    }
});

$(document).on('click','.new_payment',function(){
    $('.new_payment_card').show();
});

$( document ).ready(function() {
    $('.new_payment_card').hide();
});


$(document).on('change','.tds_deducted',function(){

    var tds_deducted = $(this).val();

    if(tds_deducted == 1){

        $('.tax_taxation').hide();
        $(".tax").attr('required',false);

    }else{

        $('.tax_taxation').show();
        $(".tax").attr('required',true);
    } 

});

$("#saveEditedPaymentDetail").validate({
    errorElement: 'span',
    rules: {
        date_of_entery: {
            required: true
        },
        paid_amount:{
            required: true  
        },
        tax_amount:{
            required: true  
        },
        tds_deducted:{
            required: true,
        }
    },
    messages: {
        date_of_entery: {
            required: "Select Date Of Entry"
        },
        paid_amount: {
            required: "Enter Paid Amount"
        },
        tax_amount:{
            required: "Enter Tax Amount"  
        },
        tds_deducted:{
            required: "Enter Tax Deducted",
        }
    }
}); 
</script>
@endsection