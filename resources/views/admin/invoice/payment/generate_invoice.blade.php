@extends('layouts.admin')
@section('title','Generate Invoice')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Generate Invoice</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.industryList') }}">Invoice List</a></li>
                            <li class="breadcrumb-item active">Generate Invoice</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>   
        <form class="custom-validation" action="{{ route('admin.saveEditedGenerateInvoice') }}" method="post" id="saveInvoice" enctype="multipart/form-data">
        @csrf
            <input type="hidden" name="id" value="{{$payment_detail->id}}">
            <input type="hidden" name="invoice_id" value="{{$find_invoice->id}}">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <label class="input-group-text">Paid Amount</label>
                                        </div>
                                        <input type="text" class="form-control" name="paid_amount" readonly value="{{$payment_detail->paid_amount}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <label class="input-group-text">Tax Amount</label>
                                        </div>
                                        <input type="text" class="form-control" name="tax_amount" readonly value="{{$payment_detail->tax_amount}}">
                                    </div>
                                </div>
                                @if($payment_detail->tds_deducted == 2)
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <label class="input-group-text">TDS</label>
                                        </div>
                                        <input type="text" class="form-control" name="tds_amount" readonly value="{{$payment_detail->tds_amount}}">
                                    </div>
                                </div>
                                @endif
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Invoice Type:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" id="example-text-input" readonly value="Tax Invoice">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Select Client:</label>
                                        <div class="col-md-8">
                                            <input class="form-control client_name" type="text" name="client_name" id="example-text-input" value="{{$find_invoice->company->company_name}}">
                                            <input type="hidden" name="client_id" class="client_id" id="client_id" value="{{$find_invoice->client_id}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Client Address:</label>
                                        <div class="col-md-8">
                                           
                                            <textarea class="form-control address" name="address" id="address" placeholder="Address" autocomplete="off" required>{{$find_invoice->address}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">GSTN:</label>
                                        <div class="col-md-8">
                                            <input class="form-control gstn" name="gstn" type="text" readonly id="example-text-input" value="{{$find_invoice->gstn}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Project Incharge:</label>
                                        <div class="col-md-8">
                                            <select class="select2 form-control select2-multiple company_consultant" multiple="multiple" name="consultant[]" data-placeholder="Select Consultant" required>
                                                @forelse ($get_consultant as $gk => $gv)
                                                    <option value="{{$gv->id}}" @if(in_array($gv->id,$find_incharge)) selected @endif>{{$gv->name}}</option>
                                                @empty
                                                    <option>No Data Found</option>
                                                @endforelse
                                            </select>
                                            <span id="consultant"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6 text-right">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Invoice No:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" name="invoice_no" id="example-text-input" readonly value="{{$payment_detail->id}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Invoice Date:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="invoice_date" placeholder="dd/mm/yyyy" data-provide="datepicker" autocomplete="off" data-date-autoclose="true" data-date-format="dd/mm/yyyy" @if($find_invoice->invoice_date != '') value="{{ date('d/m/yy',strtotime($find_invoice->invoice_date)) }}" @endif  data-date-end-date="0d">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">Due Date:</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="due_date" placeholder="dd/mm/yyyy" data-provide="datepicker" autocomplete="off" data-date-autoclose="true" data-date-format="dd/mm/yyyy" @if($find_invoice->due_date != '') value="{{ date('d/m/yy',strtotime($find_invoice->due_date)) }}" @endif data-date-end-date="0d">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">State of Supply</label>
                                        <div class="col-md-8">
                                            <input class="form-control state" name="state_of_supply" type="text" readonly id="example-text-input" value="{{$find_invoice->state->state}}" >
                                            <input type="hidden" name="state_id" class="state_id" value="{{$find_invoice->state_of_supply}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-md-3 col-form-label">State Code:</label>
                                        <div class="col-md-8">
                                            <input class="form-control state_code" name="state_code" readonly type="text" id="example-text-input" value="{{$find_invoice->state_code}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-6 mt-3">
                                    <address>
                                        <strong>Payment Method:</strong><br>
                                        Visa ending **** 4242<br>
                                        jsmith@email.com
                                    </address>
                                </div>
                                <div class="col-6 mt-3 text-right">
                                    <address>
                                        <strong>Order Date:</strong><br>
                                        October 16, 2019<br><br>
                                    </address>
                                </div>
                            </div> -->
                            <div class="py-2 mt-3">
                                <h3 class="font-size-15 font-weight-bold">Bill Details</h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-nowrap table-bordered new_link_service">
                                    <thead>
                                        <tr>
                                            <!-- <th style="width: 70px;">No.</th> -->
                                            <th>Service</th>
                                            <th>HSN</th>
                                            <th>Description</th>
                                            <th>QTY</th>
                                            <th>Rate</th>
                                            <th>Subtotal</th>
                                            <th>Tax</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 0; @endphp
                                        @if(!is_null($find_invoice->service))
                                            @foreach($find_invoice->service as $sk => $sv)
                                                <tr class="new_card service_{{$sk}}">
                                                    <td style="width: 162px;">
                                                        <input type="text" class="form-control service_name" id="service_{{$sk}}" name="service[{{$sk}}][name]" data-msg="Enter Service" data-id="{{$sk}}" data-id="" value="{{$sv->name}}"  autocomplete="off" required/>
                                                        <input type="hidden" class="service" name="service[{{$sk}}][service_id]" id="service_id_{{$sk}}" data-id="{{$sk}}" value="{{$sv->service_id}}">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" data-msg="Enter HSN" name="service[{{$sk}}][hsn]"  autocomplete="off" value="{{$sv->hsn}}" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="service[{{$sk}}][description]" value="{{$sv->description}}" autocomplete="off" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control qty number" data-msg="Enter Quantity" id="qty_{{$sk}}" name="service[{{$sk}}][qty]" data-id="{{$sk}}"  autocomplete="off" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control rate number" data-msg="Enter Rate" id="rate_{{$sk}}" name="service[{{$sk}}][rate]" data-id="{{$sk}}"  autocomplete="off" required/>
                                                    </td>   
                                                    <td>
                                                        <input type="text" class="form-control number subtotal" id="subtotal_{{$sk}}" data-id="{{$sk}}" name="service[{{$sk}}][subtotal]"  autocomplete="off" readonly="readonly" />
                                                    </td>

                                                    <td class="text-right">
                                                        <input type="text" class="form-control number tax" id="tax_{{$sk}}" data-id="{{$sk}}" name="service[{{$sk}}][tax]" autocomplete="off" value="{{$sv->tax}}" data-msg="Enter Tax" required/>

                                                        <input type="hidden" class="total_tax_payment" name="service[{{$sk}}][total_tax_payment]" id="total_tax_payment_{{$sk}}" data-id="{{$sk}}" value="{{$sv->total_tax_payment}}">

                                                        <input type="hidden" class="total_sgst_payment" name="service[{{$sk}}][total_sgst_payment]" id="total_sgst_payment_{{$sk}}" data-id="{{$sk}}" value="{{$sv->total_sgst_payment}}">

                                                        <br>
                                                        <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light same_state" @if($find_invoice->tax_type == 3 || $find_invoice->tax_type == 2) style="display: none;" @endif>GST</button>

                                                        <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light different_state" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 3) style="display: none;" @endif>IGST</button>

                                                        <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light international" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 2) style="display: none;" @endif>INT</button>

                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="text-danger removeDivContact" data-toggle="tooltip" data-id="1" data-value="{{$sk}}" data-placement="top" title="" data-original-title="Remove"><i class="mdi mdi-close font-size-20"></i></a> 
                                                    </td>
                                                </tr>

                                                @php $i++; @endphp
                                            @endforeach
                                        @endif
                                    </tbody>                                    
                                </table>
                                
                                            
                                            <!-- <td colspan="7" class="text-right">Sub Total</td>
                                            <td class="text-right">
                                            <input type="text" class="form-control number total_sub_total" readonly name="total_sub_total" value="$00.00" autocomplete="off"/> -->
                                            <!-- </td> -->
                                        
                                        <!-- <tr>
                                            <td colspan="7" class="border-0 text-right">
                                                <strong>Tax Amount</strong></td>
                                            <td class="border-0 text-right">$00.00</td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="border-0 text-right">
                                                <strong>Total Payable</strong></td>
                                            <td class="border-0 text-right"><h4 class="m-0">$00.00</h4></td>
                                        </tr> -->
                                    
                            </div>
                            <div class="form-group row">
                                
                                    <a href="javascript:void(0);" class="text-primary addNewService" data-toggle="tooltip" data-id="{{ $i + 1}}" data-value="{{ $i }}" data-placement="top" title="" data-original-title="Add"><i class="mdi mdi-plus font-size-20"></i></a> 
                                
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Sub Total:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_sub_total" type="text" name="total_sub_total" id="example-text-input" value="" readonly>
                                </div>
                            </div>

                            <div class="form-group row" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 2) style="display: none;" @endif>
                                <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Tax Amount:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_tax_amount" type="text" name="total_tax_amount" id="example-text-input" value="" readonly>
                                    <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                </div>
                            </div>

                            <div class="form-group row same_state" @if($find_invoice->tax_type == 3 || $find_invoice->tax_type == 2) style="display: none;" @endif>
                                <label for="example-text-input" class="col-md-6 col-form-label text-right"></label>
                                <label for="example-text-input" class="col-md-1 col-form-label text-right"><strong>SGST:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_sgst" type="text" name="total_sgst"  id="example-text-input" value="" readonly>
                                    <input type="hidden" name="sgst" class="sgst" value="">
                                    <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                </div>
                                <label for="example-text-input" class="col-md-1 col-form-label text-right"><strong>CGST:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_cgst" type="text" name="total_cgst"  id="example-text-input" value="" readonly>
                                    <input type="hidden" name="cgst" class="cgst" value="">
                                </div>
                            </div>

                            <div class="form-group row different_state" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 3) style="display: none;" @endif>
                                
                                <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>IGST:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_tax_amount" type="text" name="total_tax_amount"  id="example-text-input" value="" readonly>
                                    <input type="hidden" name="igst" class="igst" value="">
                                    <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Total Payable:</strong></label>
                                <div class="col-md-2">
                                    <input class="form-control number total_payable" type="text" name="total_payable" value="" id="example-text-input" readonly>
                                </div>
                            </div>
                            <div class="d-print-none">
                                <div class="float-right">
                                    <input type="submit" name="submit" class="btn btn-primary w-md waves-effect waves-light">
                                    <a href="{{ route('admin.invoiceList') }}" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>
//service suggestion
$(document).on('keyup','.service_name',function(){
    var id = $(this).data('id');
    var state_code = $('.state_code').val();
    var client_id = $('.client_id').val();
    
    $.ajax({
        url: "/administrator-panel/invoice/get-service",
        type: "POST",
        data:{ 
            'state_code' : state_code,
            'client_id' : client_id 
        },
        dataType: "JSON",
        success: function(data){
            autocompletedatalist = data;
            $('#service_'+id).autocomplete({ 
                source: autocompletedatalist,
                select: function(event, ui) {
                    $('#service_'+id).val(ui.item.label);
                    $('#service_id_'+id).val(ui.item.value);
                    $('#tax_'+id).val(ui.item.tax);
                    $('#hsn_'+id).val(ui.item.hsn);
                    $('.tax_type').val(ui.item.tax_type);

                    if(ui.item.tax_type == 1){

                        $('.sgst').val(ui.item.sgst);
                        $('.cgst').val(ui.item.cgst);
                        $('.same_state').show();
                        $('.different_state').hide();
                        $('.international').hide();

                    }else if(ui.item.tax_type == 2){

                        $('.different_state').show();
                        $('.same_state').hide();
                        $('.international').hide();

                    }else{

                        $('.international').show();
                        $('.same_state').hide();
                        $('.different_state').hide();

                    }

                    return false;
                },
            });
        }
    });
});

$(document).on('click','.removeDivContact',function(){ 
    $(this).closest('.new_card').remove();
});


//add service 
$(document).on('click','.addNewService',function(){

    var service_id = $(this).data('id');
    var servicevalue = $(this).data('value');

    var pro = '<tbody class="removeRow"><tr><td style="width: 162px;"><input type="text" class="form-control service_name" id="service_'+service_id+'" name="service['+service_id+'][name]" data-msg="Enter Service Name" data-id="'+service_id+'" autocomplete="off" required/><input type="hidden" class="service" name="service['+service_id+'][service_id]" id="service_id_'+service_id+'" data-id="'+service_id+'"></td><td><input type="text" class="form-control"  name="service['+service_id+'][hsn]" id="hsn_'+service_id+'" data-msg="Enter HSN" data-id="'+service_id+'" autocomplete="off" " required/></td><td><input type="text" class="form-control" id="service_'+service_id+'" name="service['+service_id+'][description]" data-id="'+service_id+'"  autocomplete="off"/></td><td><input type="text" class="form-control number qty" id="qty_'+service_id+'" name="service['+service_id+'][qty]" data-id="'+service_id+'" data-msg="Enter Quantity" autocomplete="off" required/></td><td><input type="text" class="form-control number rate" data-msg="Enter Rate" id="rate_'+service_id+'" name="service['+service_id+'][rate]" data-id="'+service_id+'" autocomplete="off" required/></td><td><input type="text" class="form-control number subtotal" name="service['+service_id+'][subtotal]" id="subtotal_'+service_id+'" data-id="'+service_id+'" autocomplete="off"/></td><td class="text-right"><input type="text" class="form-control number tax" data-msg="Enter Tax" id="tax_'+service_id+'" name="service['+service_id+'][tax]" data-id="'+service_id+'" autocomplete="off" required/><input type="hidden" name="service['+service_id+'][total_tax_payment]" class="total_tax_payment" id="total_tax_payment_'+service_id+'" data-id="'+service_id+'" ><input type="hidden" class="total_sgst_payment" name="service['+service_id+'][total_sgst_payment]" id="total_sgst_payment_'+service_id+'" data-id="'+service_id+'" ><br><button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light same_state" style="display:none;">GST</button><button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light different_state" style="display:none;">IGST</button><button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light international" style="display:none;">INT</button></td><td><a href="javascript:void(0);" class="text-danger remove" data-toggle="tooltip" data-id="+service_id+"  data-placement="top" title="" data-original-title="Remove"><i class="mdi mdi-close font-size-20"></i></a></td></tr></tbody>';

    $('.new_link_service').append(pro);
    service_id++; servicevalue++;
    $(this).data('value',servicevalue);
    $(this).data('id',service_id);
});

$(document).on('click','.remove',function(){ 
    var id = $('#service_id_'+$(this).data('id')).val();
    $(this).closest('.removeRow').remove();
   
});

var selected = [];

$(document).ready(function(){
    $(".service").each(function(){
        $('selected').push($(this).val());
    });
});

$(document).on('click','.remove',function(){ 
    var id = $('#service_id_'+$(this).data('id')).val();
    $(this).closest('.removeRow').remove();
    selected.splice($.inArray(id, selected), 1);
});

$(document).on('focusout','.service_name',function(){
    var id = $('#service_id_'+$(this).data('id')).val();

    if(jQuery.inArray(id, selected) !== -1){
        
        toastr.error('You can not link same service');
        $(this).val('');
        $('#service_id_'+$(this).data('id')).val('');

    } else {

       selected.push(id);

    }
});

//client detail 
$(document).on('keyup','.client_name',function(){
    var id = $(this).data('id');

    $.ajax({
        url: "/administrator-panel/get-clients",
        type: "POST",
        dataType: "JSON",
        success: function(data){
            autocompletedatalist = data;
            $('.client_name').autocomplete({ 
                source: autocompletedatalist,
                focus: function(event, ui) {
                    event.preventDefault();
                    this.client_id = ui.item.label;
                    this.address = ui.item.label;
                    this.gstn = ui.item.label;
                    this.state_id = ui.item.label;
                    this.state = ui.item.label;
                    this.state_code = ui.item.label;
                },
                select: function(event, ui) {
                    event.preventDefault();
                    $('.client_name').val(ui.item.label);
                    $('.client_id').val(ui.item.client_id);
                    $('.address').val(ui.item.address);
                    $('.gstn').val(ui.item.gstn);
                    $('.state').val(ui.item.state);
                    $('.state_id').val(ui.item.state_id);
                    $('.state_code').val(ui.item.state_code);
                    console.log(ui.item.consultants);
                    $('.company_consultant').val(ui.item.consultants).trigger('change');
                    return false;
                },
            });
        }
    });
});

$(document).on('keyup', '.qty', function() {

    var qty = $(this).val();
    var id = $(this).data('id');
    var rate = $('#rate_'+id).val();
    var subtotal = parseInt(qty) * parseInt(rate);

    $('#subtotal_'+id).val(subtotal);

    var tax = $('#tax_'+id).val(); 

    var subtotal_per_tax = parseInt(tax) * parseInt(subtotal);
    var total_tax_payment = subtotal_per_tax / 100;

    $('#total_tax_payment_'+id).val(total_tax_payment);

    var sgst_payment = parseInt(total_tax_payment) / 2;
    $('#total_sgst_payment_'+id).val(sgst_payment);

    var sum = 0;
    $(".subtotal").each(function(){
         sum += +$(this).val();
    });
    $(".total_sub_total").val(sum);
    
    var tax_type = $('.tax_type').val(); 

    if(tax_type == 2 || tax_type ==3){

        var tax_payment = 0;
        $(".total_tax_payment").each(function(){
            tax_payment += +$(this).val();
        });
        $(".total_tax_amount").val(tax_payment);

        var total_payable = parseInt(tax_payment) + parseInt(sum);

    }else if(tax_type == 1){

        var new_sgst_payment = 0;
        $(".total_sgst_payment").each(function(){
            new_sgst_payment += +$(this).val();
        });
        $(".total_cgst").val(new_sgst_payment);
        $(".total_sgst").val(new_sgst_payment);

        var total_payable = parseInt(new_sgst_payment) + parseInt(new_sgst_payment) + parseInt(sum);
    }
    $(".total_payable").val(total_payable);

});

$(document).on('keyup', '.rate', function() {

    var rate = $(this).val();
    var id = $(this).data('id');
    var qty = $('#qty_'+id).val();    
    var subtotal = parseInt(qty) * parseInt(rate);

    $('#subtotal_'+id).val(subtotal);

    var tax = $('#tax_'+id).val(); 

    var subtotal_per_tax = parseInt(tax) * parseInt(subtotal);
    var total_tax_payment = subtotal_per_tax / 100;

    $('#total_tax_payment_'+id).val(total_tax_payment);

    var sgst_payment = parseInt(total_tax_payment) / 2;
    $('#total_sgst_payment_'+id).val(sgst_payment);

    var sum = 0;
    $(".subtotal").each(function(){
         sum += +$(this).val();
    });
    $(".total_sub_total").val(sum);

    var tax_type = $('.tax_type').val(); 

    if(tax_type == 2 || tax_type ==3){

        var tax_payment = 0;
        $(".total_tax_payment").each(function(){
            tax_payment += +$(this).val();
        });
        $(".total_tax_amount").val(tax_payment);

        var total_payable = parseInt(tax_payment) + parseInt(sum);

    }else if(tax_type == 1){

        var new_sgst_payment = 0;
        $(".total_sgst_payment").each(function(){
            new_sgst_payment += +$(this).val();
        });
        $(".total_cgst").val(new_sgst_payment);
        $(".total_sgst").val(new_sgst_payment);

        var total_payable = parseInt(new_sgst_payment) + parseInt(new_sgst_payment) + parseInt(sum);
    }
    $(".total_payable").val(total_payable);
});

$(document).on('keyup', '.tax', function() {

    var tax = $(this).val();
    var id = $(this).data('id');
    var qty = $('#qty_'+id).val();;
    var rate = $('#rate_'+id).val();

    var subtotal = parseInt(qty) * parseInt(rate);

    $('#subtotal_'+id).val(subtotal);

    var tax = $('#tax_'+id).val(); 

    var subtotal_per_tax = parseInt(tax) * parseInt(subtotal);
    var total_tax_payment = subtotal_per_tax / 100;

    $('#total_tax_payment_'+id).val(total_tax_payment);

    var sgst_payment = parseInt(total_tax_payment) / 2;
    $('#total_sgst_payment_'+id).val(sgst_payment);
    
    var sum = 0;
    $(".subtotal").each(function(){
         sum += +$(this).val();
    });
    $(".total_sub_total").val(sum);

    var tax_type = $('.tax_type').val(); 

    if(tax_type == 2 || tax_type ==3){

        var tax_payment = 0;
        $(".total_tax_payment").each(function(){
            tax_payment += +$(this).val();
        });
        $(".total_tax_amount").val(tax_payment);

        var total_payable = parseInt(tax_payment) + parseInt(sum);

    }else if(tax_type == 1){

        var new_sgst_payment = 0;
        $(".total_sgst_payment").each(function(){
            new_sgst_payment += +$(this).val();
        });
        $(".total_cgst").val(new_sgst_payment);
        $(".total_sgst").val(new_sgst_payment);

        var total_payable = parseInt(new_sgst_payment) + parseInt(new_sgst_payment) + parseInt(sum);
    }
    $(".total_payable").val(total_payable);

});
</script>
@endsection