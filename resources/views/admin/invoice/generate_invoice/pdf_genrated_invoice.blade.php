<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
       
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

        <!-- Bootstrap Css -->
        <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- <link href="{{ asset('plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> -->

        <!-- Responsive datatable examples -->
       <!--  <link href="{{ asset('plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />     -->
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.min.css" rel="stylesheet"></link> -->
    </head>

    <body data-sidebar="dark">
        <div id="layout-wrapper">
            
            <div class="page-content">
                <div class="container-fluid">
                    <!-- start page title -->
                    
                        <input type="hidden" name="id" value="{{$find_invoice->id}}">
                        <input type="hidden" name="invoice_id" value="{{$find_invoice->invoice_id}}">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <label class="input-group-text">Paid Amount</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="paid_amount" readonly value="{{$find_invoice->paid_amount}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <label class="input-group-text">Tax Amount</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="tax_amount" readonly value="{{$find_invoice->tax_amount}}">
                                                </div>
                                            </div>
                                            @if($find_invoice->tds_amount != '')
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <label class="input-group-text">TDS</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="tds_amount" readonly value="{{$find_invoice->tds_amount}}">
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Invoice Type:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" id="example-text-input" readonly value="Tax Invoice">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Select Client:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control client_name" type="text" name="client_name" id="example-text-input" value="{{$find_invoice->company->company_name}}">
                                                        <input type="hidden" name="client_id" class="client_id" id="client_id"  value="{{$find_invoice->client_id}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Client Address:</label>
                                                    <div class="col-md-8">
                                                       
                                                        <textarea class="form-control address" name="address" id="address" placeholder="Address" autocomplete="off" required>{{$find_invoice->address}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">GSTN:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control gstn" name="gstn" type="text" id="example-text-input" value="{{$find_invoice->gstn}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Project Incharge:</label>
                                                    <div class="col-md-8">
                                                        <select class="select2 form-control select2-multiple" multiple="multiple" name="consultant[]" data-placeholder="Select Consultant" required>
                                                            @forelse ($get_consultant as $gk => $gv)
                                                                <option value="{{$gv->id}}" @if(in_array($gv->id,$find_incharge)) selected @endif>{{$gv->name}}</option>
                                                            @empty
                                                                <option>No Data Found</option>
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6 text-right">
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Invoice No:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control" type="text" name="invoice_no" id="example-text-input" readonly value="{{$find_invoice->invoice_no}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Invoice Date:</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="invoice_date" placeholder="dd/mm/yyyy"  autocomplete="off" data-date-autoclose="true" data-date-format="dd/mm/yyyy" @if($find_invoice->invoice_date != '') value="{{ date('d/m/yy',strtotime($find_invoice->invoice_date)) }}" @endif  data-date-end-date="0d">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">Due Date:</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="due_date" placeholder="dd/mm/yyyy"  autocomplete="off" data-date-autoclose="true" data-date-format="dd/mm/yyyy" @if($find_invoice->due_date != '') value="{{ date('d/m/yy',strtotime($find_invoice->due_date)) }}" @endif data-date-end-date="0d">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">State of Supply</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control state" name="state_of_supply" type="text" id="example-text-input" readonly value="{{$find_invoice->state->state}}" >
                                                        <input type="hidden" name="state_id" class="state_id" value="{{$find_invoice->state_of_supply}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-3 col-form-label">State Code:</label>
                                                    <div class="col-md-8">
                                                        <input class="form-control state_code" name="state_code" readonly type="text" id="example-text-input" value="{{$find_invoice->state_code}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="py-2 mt-3">
                                            <h3 class="font-size-15 font-weight-bold">Bill Details</h3>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-nowrap table-bordered new_link_service">
                                                <thead>
                                                    <tr>
                                                        <!-- <th style="width: 70px;">No.</th> -->
                                                        <th>Service</th>
                                                        <th>HSN</th>
                                                        <th>Description</th>
                                                        <th>QTY</th>
                                                        <th>Rate</th>
                                                        <th>Subtotal</th>
                                                        <th>Tax</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 0; @endphp
                                                    @if(!is_null($find_invoice->service))
                                                        @foreach($find_invoice->service as $sk => $sv)
                                                            <tr class="new_card service_{{$sk}}">
                                                                <td style="width: 162px;">
                                                                    <input type="text" class="form-control service_name" id="service_{{$sk}}" name="service[{{$sk}}][name]" data-msg="Enter Service" data-id="{{$sk}}" data-id="" value="{{$sv->name}}"  autocomplete="off" required/>
                                                                    <input type="hidden" class="service" name="service[{{$sk}}][service_id]" id="service_id_{{$sk}}" data-id="{{$sk}}" value="{{$sv->service_id}}">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" data-msg="Enter HSN" name="service[{{$sk}}][hsn]"  autocomplete="off" value="{{$sv->hsn}}" required/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="service[{{$sk}}][description]" value="{{$sv->description}}" autocomplete="off"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control qty number" data-msg="Enter Quantity" id="qty_{{$sk}}" name="service[{{$sk}}][qty]" data-id="{{$sk}}" value="{{$sv->quantity}}" autocomplete="off" required/>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control rate number" data-msg="Enter Rate" id="rate_{{$sk}}" name="service[{{$sk}}][rate]" data-id="{{$sk}}" value="{{$sv->rate}}" autocomplete="off" required/>
                                                                </td>   
                                                                <td>
                                                                    <input type="text" class="form-control number subtotal"  id="subtotal_{{$sk}}" data-id="{{$sk}}" name="service[{{$sk}}][subtotal]" value="{{$sv->subtotal}}" autocomplete="off" readonly="readonly"/>
                                                                </td>

                                                                <td class="text-right">

                                                                    <input type="text" class="form-control number tax" id="tax_{{$sk}}" data-id="{{$sk}}" name="service[{{$sk}}][tax]" autocomplete="off" value="{{$sv->tax}}" data-msg="Enter Tax" required/>

                                                                    <input type="hidden" class="total_tax_payment" name="service[{{$sk}}][total_tax_payment]" id="total_tax_payment_{{$sk}}" data-id="{{$sk}}" value="{{$sv->total_tax_payment}}">

                                                                    <input type="hidden" class="total_sgst_payment" name="service[{{$sk}}][total_sgst_payment]" id="total_sgst_payment_{{$sk}}" data-id="{{$sk}}" value="{{$sv->total_sgst_payment}}">

                                                                    <br>
                                                                    <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light same_state" @if($find_invoice->tax_type == 3 || $find_invoice->tax_type == 2) style="display: none;" @endif>GST</button>

                                                                    <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light different_state" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 3) style="display: none;" @endif>IGST</button>

                                                                    <button type="button" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light international" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 2) style="display: none;" @endif>INT</button>

                                                                </td>
                                                                <td>
                                                                    <a href="javascript:void(0);" class="text-danger removeDivContact" data-toggle="tooltip" data-id="1" data-value="{{$sk}}" data-placement="top" title="" data-original-title="Remove"><i class="mdi mdi-close font-size-20"></i></a> 
                                                                </td>
                                                            </tr>

                                                            @php $i++; @endphp
                                                        @endforeach
                                                    @endif
                                                </tbody>                                    
                                            </table>
                                        </div>
                                        <div class="form-group row">
                                            
                                                <a href="javascript:void(0);" class="text-primary addNewService" data-toggle="tooltip" data-id="{{ $i + 1}}" data-value="{{ $i }}" data-placement="top" title="" data-original-title="Add"><i class="mdi mdi-plus font-size-20"></i></a> 
                                            
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Sub Total:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_sub_total" type="text" name="total_sub_total" id="example-text-input" value="{{$find_invoice->total_sub_total}}" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group row" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 2) style="display: none;" @endif>
                                            <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Tax Amount:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_tax_amount" type="text" name="total_tax_amount" id="example-text-input" value="{{$find_invoice->total_tax_amount}}" readonly>
                                                <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                            </div>
                                        </div>

                                        <div class="form-group row same_state" @if($find_invoice->tax_type == 3 || $find_invoice->tax_type == 2) style="display: none;" @endif>
                                            <label for="example-text-input" class="col-md-6 col-form-label text-right"></label>
                                            <label for="example-text-input" class="col-md-1 col-form-label text-right"><strong>SGST:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_sgst" type="text" name="total_sgst"  id="example-text-input" value="{{$find_invoice->total_sgst}}" readonly>
                                                <input type="hidden" name="sgst" class="sgst" value="">
                                                <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                            </div>
                                            <label for="example-text-input" class="col-md-1 col-form-label text-right"><strong>CGST:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_cgst" type="text" name="total_cgst"  id="example-text-input" value="{{$find_invoice->total_cgst}}" readonly>
                                                <input type="hidden" name="cgst" class="cgst" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row different_state" @if($find_invoice->tax_type == 1 || $find_invoice->tax_type == 3) style="display: none;" @endif>
                                            
                                            <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>IGST:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_tax_amount" type="text" name="total_tax_amount"  id="example-text-input" value="{{$find_invoice->total_tax_amount}}" readonly>
                                                <input type="hidden" name="igst" class="igst" value="">
                                                <input type="hidden" name="tax_type" class="tax_type" value="{{$find_invoice->tax_type}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-10 col-form-label text-right"><strong>Total Payable:</strong></label>
                                            <div class="col-md-2">
                                                <input class="form-control number total_payable" type="text" name="total_payable" value="{{$find_invoice->total_payable}}" id="example-text-input" readonly>
                                            </div>
                                        </div>
                                        <div class="d-print-none">
                                            <div class="float-right">
                                                <input type="submit" name="submit" class="btn btn-primary w-md waves-effect waves-light">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
            </div>
           
        </div>
        <div class="rightbar-overlay"></div>

        <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
        <!-- <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script> -->
        <!-- <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/metismenu/metisMenu.min.js') }}"></script>
        <script src="{{ asset('plugins/simplebar/simplebar.min.js') }}"></script>
        <script src="{{ asset('plugins/node-waves/waves.min.js') }}"></script>   -->
        
        <script src="{{ asset('js/app.js') }}"></script>
        <!-- Required datatable js -->
        <!-- <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script> -->
        <script src="{{ asset('plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

        <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
        
        <script src="{{ asset('js/pages/form-advanced.init.js') }}"></script>

        <!-- <script src="{{ asset('js/common.js') }}"></script> -->
    </body>
</html>
