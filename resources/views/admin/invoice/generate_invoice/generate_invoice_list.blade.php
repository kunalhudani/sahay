@extends('layouts.admin')
@section('title','Genrated Invoice')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Genrated Invoice</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.invoiceList') }}">Invoice List</a></li>
                            <li class="breadcrumb-item active">Genrated Invoice</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Invoice No</th>
                                    <th>Company Name</th>  
                                    <th>Project Incharge</th> 
                                    <th>Invoice Amount</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($get_genrated_invoices))
                                @foreach($get_genrated_invoices as $gk => $gv)
                                    <tr>
                                        <td>{{ $gv->invoice_no }}</td>
                                        <td>{{ $gv->company->company_name }}</td>
                                        
                                        @if(!is_null($gv['consultant']))
                                            @php $consultant = array(); @endphp
                                            @foreach($gv['consultant'] as $ck => $cv)
                                                @if($cv != '')
                                                   @php $consultant[] = $cv->project_incharge_name->name; @endphp 
                                                @endif
                                            @endforeach
                                            <td>{{ implode(' | ',$consultant) }}</td>
                                        @else
                                            <td> -------- </td>
                                        @endif

                                        
                                        <td>{{ $gv->total_payable }}</td>
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.sendGeneratedInvoice',['id' => $gv->id, 'invoice_id' => $id]) }}" title="Send Invoice" role="button"><i class="mdi mdi-email-send"></i></a>

                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editGenratedInvoice',['id' => $gv->id, 'invoice_id' => $id]) }}" title="Edit Genrated Invoice" role="button"><i class="bx bx-pencil"></i></a>

                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteGeneratedInvoice',['id' => $gv->id, 'invoice_id' => $id]) }}" title="Delete Service" role="button" onclick="return confirm('Do you want to delete this Genrated invoice?');"><i class="bx bx-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
@endsection
@section('js')

@endsection