@extends('layouts.admin')
@section('title','All Invoice')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">All Invoice</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">All Invoice</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{ route('admin.sendClientReminder')}}" method="post" id="sendClientReminder">
                            @csrf
                    <a href="javascript:void(0);" class="btn btn-primary mb-3" id="sendReminder" name="save_and_list" value="save_and_list" style="float:right;margin-right: 10px;margin-top: 5px;">Send Reminder</a>
                            <br/><br/>
                    <div class="card-body">
                        
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkbox" class="checkall"></th>
                                    <th>Proforma Company</th>
                                    <th>Invoice Date</th>  
                                    <th>Due Date</th> 
                                    <th>Expected Payment Date</th> 
                                    <th>Outstanding Amount</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($get_invoice))
                                @foreach($get_invoice as $gk => $gv)
                                    <tr>

                                        <td><input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $gv->id }}"></td>
                                        <td>{{ $gv->company->company_name }}</td>
                                        <td>{{ date('d/m/y',strtotime($gv->invoice_date)) }}</td>
                                        <td>{{ date('d/m/y',strtotime($gv->due_date)) }}</td>
                                        <td>
                                            <input type="text" class="form-control expected_date date_{{$gv->id}}" name="date_of_entery" placeholder="dd/mm/yyyy" autocomplete="off"  data-date-autoclose="true" data-provide="datepicker" data-msg="Select Date Of Entry" data-date-format="dd/mm/yyyy" data-id="{{$gv->id}}" @if($gv->expected_payment_date != '') data-value="{{ date('d/m/yy',strtotime($gv->expected_payment_date)) }}" @endif @if($gv->expected_payment_date != '') value="{{ date('d/m/yy',strtotime($gv->expected_payment_date)) }}" @endif data-date-end-date="0d">

                                            <div class="editable-buttons save_{{$gv->id}}" style="display: none;">
                                                <button type="button" class="btn btn-primary bx bx-check-double add_{{$gv->id}}" data-id="{{$gv->id}}"><i class="glyphicon glyphicon-ok"></i></button>
                                                <button type="button" class="btn btn-default btn-sm editable-cancel remove_{{$gv->id}}" data-id="{{$gv->id}}"><i class="mdi mdi-close font-size-18"></i></button>
                                            </div>
                                        </td>
                                        
                                        <td>
                                        @if(!empty($payment))
                                            @foreach($payment as $pk => $pv)
                                                @if($gv->id == $pk)
                                                    <?php $sum_payment = array_sum($pv);
                                                        $total = $gv->total_payable - $sum_payment;
                                                    ?>
                                                    
                                                @else
                                                    <?php $total = $gv->total_payable;?>
                                                @endif
                                            @endforeach

                                            {{$total}}
                                        @else
                                            {{$gv->total_payable}}
                                        @endif
                                            
                                     
                                        </td>
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editInvoice',$gv->id) }}" title="Edit Service" role="button"><i class="bx bx-pencil"></i></a>

                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.sendPerforma',$gv->id) }}" title="Send Performa" role="button"><i class="mdi mdi-email-send"></i></a>

                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.paymentDetail',$gv->id) }}" title="Payment Details" role="button"><i class="bx bx-credit-card"></i></a>

                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.genratedInvoiceList',$gv->id) }}" title="Invoice List" role="button"><i class="bx bx-copy-alt"></i></a>

                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.reminderList',$gv->id) }}" title="Reminder Log" role="button"><i class="bx bx-timer"></i></a>

                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteInvoice',$gv->id) }}" title="Delete Service" role="button" onclick="return confirm('Do you want to delete this invoice?');"><i class="bx bx-trash-alt"></i></a>

                                            
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    </form>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
@endsection
@section('js')
<script type="text/javascript">
 $(document).on('change','.checkall',function(){
    if(this.checked){
        $('.checkbox').prop('checked',true);
    } else {
        $('.checkbox').prop('checked',false);
    }
});

$(document).on('click','#sendReminder',function(){
    var selected = [];
    $(".checkbox:checked").each(function(){
        selected.push($(this).val());
    });

    if(selected.length){
        $('#sendClientReminder').submit();
    } else {
        toastr.error('Please Select Atleast One Invoice');
    }
});

$(document).on('click','.expected_date',function(){
    var id = $(this).data('id');
    $('.save_'+id).show();
});   

$(document).on('click','.bx-check-double',function(){
    var id = $(this).data('id');
    var expected_date = $('.date_'+id).val();
    console.log(expected_date);

    $.ajax({
        url: "/administrator-panel/invoice/save-expected",
        type: "POST",
        data:{ 
            'expected_date' : expected_date,
            'id' : id
        },
        success: function(data){
            if(data == 'true'){
                toastr.success('Expected Date Successfully Updated');
                $('.save_'+id).hide();
            }else{
                toastr.error('Something Wrong');
            }
        }
    });

});
$(document).on('click','.editable-cancel',function(){
    var id = $(this).data('id');
    $('.save_'+id).hide();
    
    var value = $('.date_'+id).data('value');

    $('.date_'+id).val(value);
});  

</script>
@endsection