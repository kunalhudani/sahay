@extends('layouts.admin')
@section('title','All Reminder')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">All Reminder</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.invoiceList') }}">Invoice List</a></li>
                            <li class="breadcrumb-item active">All Reminder</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-body">
                        <a href="javascript:void(0);" class="btn btn-primary mb-3" id="sendReminder" name="save_and_list" value="save_and_list" style="float:right;;margin-right: 10px;">Send Reminder</a>
                            <br/><br/>
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    
                                    <th>Proforma Company</th>
                                    <th>Reminder Date</th> 
                                    <th>Employee Name</th>
                                    <th>Email ID</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($get_reminder))
                                @foreach($get_reminder as $gk => $gv)
                                    <tr>
                                        <td>{{ $gv->company->company_name }}</td>
                                        <td>{{ date('d/m/y h:i:s',strtotime($gv->created_at)) }}</td>
                                        <td>{{ $gv->employee->name }}</td>
                                        <td>{{ $gv->employee->email }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
@endsection
@section('js')

@endsection