@extends('layouts.admin')
@section('title','Add Department')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Add Department</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.clientDepartmentList') }}">Department List</a></li>
                            <li class="breadcrumb-item active">Add Department</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.saveClientDepartment') }}" method="post" id="clientDepartment" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Department Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="department_name" placeholder="Department Name" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                </button>
                                <button type="submit" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="save_and_update">
                                    Save & Add New
                                </button>
                                <a href="{{ route('admin.clientDepartmentList') }}" class="btn btn-secondary waves-effect">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
