@extends('layouts.admin')
@section('title','Company Profile')
@section('content')
<style type="text/css">
#upload-demo{
    width: 250px;
    height: 250px;
    padding-bottom:25px;
}
</style>
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Company Profile</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Company Profile</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.companyProfile') }}" method="post" id="companyProfile" enctype="multipart/form-data">
                            @csrf
                           
                            <input type="hidden" name="id" @if(isset($company_profile_detail->id) && ($company_profile_detail->id != '')) value="{{$company_profile_detail->id}}" @endif> 
                            <div class="form-group">
                                <label>Company Logo</label>
                                <center>
                                    <img class="rounded-circle avatar-xl" alt="200x200" width="200" @if(isset($company_profile_detail->logo) && ($company_profile_detail->logo != '')) src="{{ env('BUCKET_URL') }}{{ $company_profile_detail->uuid }}/profile_image/{{ $company_profile_detail->logo }}" @else src="{{ asset('images/companies/img-2.png') }}" @endif data-holder-rendered="true" id="item-img-output">
                                    <input type="file" name="profile" class="item-img" id="my_file" style="display: none;" />
                                    <input type="hidden" name="profile_image" id="profile_image" value="">
                                    <figure style="margin-top:-25px;margin-right:-65px;">
                                        <figcaption><i class="fa fa-camera" style="color:white"></i></figcaption>
                                    </figure>
                                </center>
                            </div>

                            <div class="form-group">
                                <label>Company Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="company_name" placeholder="Company Name" autocomplete="off" @if(isset($company_profile_detail->company_name) && ($company_profile_detail->company_name != '')) value="{{$company_profile_detail->company_name}}" @endif required/>
                            </div>

                            <div class="form-group">
                                <label>Company Address<span class="mandatory">*</span></label>
                                <textarea class="form-control" name="company_address" id="company_address" placeholder="Company Address" autocomplete="off" required>@if(isset($company_profile_detail->company_address) && ($company_profile_detail->company_address != '')) {{$company_profile_detail->company_address}} @endif</textarea>
                            </div>

                            <div class="form-group">
                                <label>State<span class="mandatory">*</span></label>
                                <input type="text" class="form-control state_name" name="company_state" placeholder="State" autocomplete="off" @if(isset($company_profile_detail->state) && ($company_profile_detail->state != '')) value="{{$company_profile_detail['state_name']['state']}}" @endif required/>
                                <input type="hidden" name="state_id" id="state_id" @if(isset($company_profile_detail->state) && ($company_profile_detail->state != '')) value="{{$company_profile_detail->state}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>State Code<span class="mandatory">*</span></label>
                                <input type="text" class="form-control state_code" name="state_code" placeholder="State Code" readonly="readonly" autocomplete="off" @if(isset($company_profile_detail->state_code) && ($company_profile_detail->state_code != '')) value="{{$company_profile_detail->state_code}}" @endif/>
                            </div>

                            <div class="form-group">
                                <label>Country<span class="mandatory">*</span></label>
                                <input type="text" class="form-control country" name="company_country" placeholder="Country" autocomplete="off" @if(isset($company_profile_detail->country) && ($company_profile_detail->country != '')) value="{{$company_profile_detail['country_detail']['country_name']}}" @endif required/>
                                <input type="hidden" name="country" id="country_id" @if(isset($company_profile_detail->country) && ($company_profile_detail->country != '')) value="{{$company_profile_detail->country}}" @endif>
                            </div>

                            <div class="form-group">
                                <label>PAN Number</label>
                                <input type="text" class="form-control alphanumeric" name="pancard_number" placeholder="PAN Number" autocomplete="off" maxlength="10" @if(isset($company_profile_detail->pancard_number) && ($company_profile_detail->pancard_number != '')) value="{{$company_profile_detail->pancard_number}}" @endif minlength="10"/>
                            </div>

                            <div class="form-group">
                                <label>GSTN</label>
                                <input type="text" class="form-control alphanumeric" name="gstn_number" placeholder="GSTN" autocomplete="off" maxlength="15" @if(isset($company_profile_detail->gstn_number) && ($company_profile_detail->gstn_number != '')) value="{{$company_profile_detail->gstn_number}}" @endif minlength="15"/>
                            </div>

                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text" class="form-control" name="bank_name" placeholder="Bank Name" @if(isset($company_profile_detail->bank_name) && ($company_profile_detail->bank_name != '')) value="{{$company_profile_detail->bank_name}}" @endif autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <label>Bank Branch</label>
                                <input type="text" class="form-control" name="bank_branch" @if(isset($company_profile_detail->bank_branch) && ($company_profile_detail->bank_branch != '')) value="{{$company_profile_detail->bank_branch}}" @endif placeholder="Bank Branch" autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <label>Account Number</label>
                                <input type="text" class="form-control number" name="account_number" @if(isset($company_profile_detail->account_number) && ($company_profile_detail->account_number != '')) value="{{$company_profile_detail->account_number}}" @endif placeholder="Account Number" autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <label>IFSC Code</label>
                                <input type="text" class="form-control alphanumeric" name="ifsc_code" placeholder="IFSC Code" autocomplete="off" maxlength="11" @if(isset($company_profile_detail->ifsc_code) && ($company_profile_detail->ifsc_code != '')) value="{{$company_profile_detail->ifsc_code}}" @endif minlength="11"/>
                            </div>

                            <div class="form-group">
                                <label>MICR </label>
                                <input type="text" class="form-control number" name="micr_number" placeholder="MICR" autocomplete="off" maxlength="9" @if(isset($company_profile_detail->micr_number) && ($company_profile_detail->micr_number != '')) value="{{$company_profile_detail->micr_number}}" @endif minlength="9"/>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                </button>
                                <a href="{{ route('admin.dashboard') }}" class="btn btn-secondary waves-effect">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
