@extends('layouts.admin')
@section('title','Company List')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Company List</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Company List</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Company Logo</th>
                                    <th>Company Name</th>
                                    <th>Mobile</th>
                                    <th>Team</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($company))
                                @foreach($company as $ek => $ev)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td width="5%"><img src="{{ env('BUCKET_URL') }}{{ $ev->uuid }}/company_logo/{{ $ev->company_logo }}" width="50%"></td>
                                        <td>{{ $ev->company_name }}</td>
                                        <td>{{ $ev->contact_no }}</td>
                                        <td>{{ count($ev->employee) }}</td>
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editCompany',$ev->id) }}" role="button" title="Edit Company"><i class="bx bx-pencil"></i></a>
                                            <a class="btn btn-info waves-effect waves-light" href="{{ route('admin.companyTeamList',$ev->uuid) }}" role="button" title="Add Team"><i class="bx bx-plus"></i></a>
                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteCompany',$ev->id) }}" title="Delete Company" role="button" onclick="return confirm('Do you want to delete this company?');"><i class="bx bx-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->
</div>
@endsection