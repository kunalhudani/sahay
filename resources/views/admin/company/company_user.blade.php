@extends('layouts.admin')
@section('title','Company Employee List')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Company Employee List</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Company Employee List</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.addCompanyUser',$uuid) }}" role="button" title="Add Employee" style="float:right;"><i class="bx bx-plus"></i> Add Employee</a><br><br><br>
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Name</th>
                                    <th>Mobile No</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($getCompanyUser))
                                @foreach($getCompanyUser as $ek => $ev)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $ev->employee->name }}</td>
                                        <td>{{ $ev->employee->mobile_no }}</td>
                                        <td>{{ $ev->employee->email }}</td>
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editCompanyUser',[$uuid,$ev->employee->id]) }}" role="button" title="Edit User"><i class="bx bx-pencil"></i></a>
                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteCompanyUser',[$uuid,$ev->employee->id]) }}" title="Delete User" role="button" onclick="return confirm('Do you want to delete this employee?');"><i class="bx bx-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->
</div>
@endsection