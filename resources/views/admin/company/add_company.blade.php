@extends('layouts.admin')
@section('title','Add Company')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Add Company</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.consultantsList') }}">Company List</a></li>
                            <li class="breadcrumb-item active">Add Company</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
        <form  action="{{ route('admin.saveCompany') }}" method="post" id="companyForm" enctype="multipart/form-data">
            @csrf
            <div class="row">

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group">
                                <label>Company Logo<span class="mandatory">*</span></label>
                                <input type="file" class="form-control dropify" name="company_logo" placeholder="Mobile Number" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label>Company Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="company_name" placeholder="Company Name" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label>Address<span class="mandatory">*</span></label>
                                <textarea class="form-control" name="address" id="address" placeholder="Address" autocomplete="off" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Mobile Number<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number" name="mobile_number" placeholder="Mobile Number" autocomplete="off" maxlength="10" minlength="10" required/>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Location<span class="mandatory">*</span></label>
                                <select class="select2 form-control select2-multiple" multiple="multiple" name="location[]" data-placeholder="Select Location" required>
                                    @forelse ($get_city as $gc => $gv)
                                        <option value="{{$gv->id}}">{{$gv->city_name}}</option>
                                    @empty
                                        <option>No Data Found</option>
                                    @endforelse
                                </select>
                                <span id="location" class="location"></span>
                            </div>

                            <div class="form-group">
                                <label>State<span class="mandatory">*</span></label>
                                <input type="text" class="form-control state_name" name="company_state" placeholder="State" autocomplete="off" required/>
                                <input type="hidden" name="state_id" id="state_id">
                            </div>

                            <div class="form-group">
                                <label>State Code<span class="mandatory">*</span></label>
                                <input type="text" class="form-control state_code" name="state_code" placeholder="State Code" readonly="readonly" autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <label>Country<span class="mandatory">*</span></label>
                                <input type="text" class="form-control country" name="company_country" placeholder="Country" autocomplete="off" required/>
                                <input type="hidden" name="country_id" id="country_id">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Industry<span class="mandatory">*</span></label>
                                <select class="form-control" name="industry_type" required>
                                    <option value="">Select Industry Type</option>
                                    @forelse ($get_industry as $gk => $gv)
                                        <option value="{{$gv->id}}">{{$gv->industry_name}}</option>
                                    @empty
                                        <option>No Data Found</option>
                                    @endforelse

                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Engagement Type<span class="mandatory">*</span></label>
                                <select class="select2 form-control select2-multiple" multiple="multiple" name="engagement[]" data-placeholder="Select Engagement Type" required>
                                @if(!is_null($get_services))
                                    @foreach($get_services as $gk => $gv)
                                        <option value="{{$gv->id}}">{{$gv->service_name}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <span id="engagement" class="engagement"></span>
                            </div>

                            <div class="form-group">
                                <label>GSTN</label>
                                <input type="text" class="form-control alphanumeric" name="gstn_number" placeholder="GSTN" autocomplete="off" maxlength="15" minlength="15"/>
                            </div>

                            <div class="form-group">
                                <label>No of Days Per Month<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number" name="days" placeholder="No of Days Per Month" autocomplete="off" required/>
                            </div>

                          <!--   <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                    </button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="save_and_update">
                                        Save & Add New
                                    </button>
                                    <a href="{{ route('admin.companyList') }}" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div> -->

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                     <div class="card">
                        <div class="card-body">
                             <div class="form-group">
                                <label class="control-label">Sahay Employees<span class="mandatory">*</span></label>
                                <select class="select2 form-control select2-multiple" multiple="multiple" name="employees[]" data-placeholder="Select Employee" required>
                                    @forelse ($get_employees as $gk => $gv)
                                        <option value="{{$gv->id}}">{{$gv->name}}</option>
                                    @empty
                                        <option>No Data Found</option>
                                    @endforelse
                                </select>
                                <span id="employees" class="employees"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label">Involved Consultant<span class="mandatory">*</span></label>
                                <select class="select2 form-control select2-multiple" multiple="multiple" name="consultant[]" data-placeholder="Select Consultant" required>
                                    @forelse ($get_consultant as $gk => $gv)
                                        <option value="{{$gv->id}}">{{$gv->name}}</option>
                                    @empty
                                        <option>No Data Found</option>
                                    @endforelse
                                </select>
                                <span id="consultant" class="consultant"></span>
                            </div>
                            
                        </div>
                    </div>
                </div>

                 <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                    </button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="save_and_update">
                                        Save & Add New
                                    </button>
                                    <a href="{{ route('admin.companyList') }}" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
</div>
@endsection
