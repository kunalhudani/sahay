@extends('layouts.admin')
@section('title','Add Company')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Add Company</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.consultantsList') }}">Company List</a></li>
                            <li class="breadcrumb-item active">Add Company</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
        <form class="custom-validation" action="{{ route('admin.saveCompanyUser') }}" method="post" id="companyUserForm" enctype="multipart/form-data">
            @csrf
            <div class="row">

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">

                            <input type="hidden" value="{{ $getCompanyId }}" name="company_id" id="company_id">
                            <input type="hidden" value="{{ $uuid }}" name="uuid">
                            <input type="hidden" value="" name="employee_id" id="employee_id">

                            <div class="form-group">
                                <label>Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="name" placeholder="Name" autocomplete="off" id="name" required/>
                            </div>

                            <div class="form-group">
                                <label>Email ID<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="email" placeholder="Email ID" autocomplete="off" id="email" required/>
                            </div>

                            <div class="form-group">
                                <label>Mobile Number<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number" name="mobile_number" placeholder="Mobile Number" autocomplete="off" maxlength="10" minlength="10" id="mobile" required/>
                            </div>

                            <div class="form-group">
                                <label>User Type<span class="mandatory">*</span></label>
                                <select class="form-control user_type" name="user_type">
                                    <option value="">Selct User Type</option>
                                    <option value="1">Owner</option>
                                    <option value="2">Employee</option>
                                </select>
                            </div>

                            <div class="form-group departments" style="display:none;">
                                <label>Department<span class="mandatory">*</span></label>
                                <select  class="form-control" id="departments" name="department" data-msg="Select Department"> 
                                    <option value="">Selct Department</option>
                                    @if(!is_null($clientDepartment))
                                        @foreach($clientDepartment as $dk => $dv)
                                            <option value="{{ $dv->id }}">{{ $dv->department_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group password">
                                <label>Password<span class="mandatory">*</span></label>
                                <input type="password" class="form-control" name="password" placeholder="Password" id="password" data-msg="Enter password" required/>
                            </div>

                            <div class="form-group password">
                                <label>Confirm Password<span class="mandatory">*</span></label>
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" id="confirm_password" data-msg="Enter confirm password" required/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                     <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Modules</h4>
                            <div class="row">
                            @if(!is_null($module))
                                @foreach($module as $mk => $mv)
                                    <div class="col-md-6">
                                        <div class="mt-4 mt-lg-0">
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" name="module[]" class="custom-control-input" id="customCheck{{$mk}}" value="{{ $mv->id }}">
                                                <label class="custom-control-label" for="customCheck{{$mk}}">{{ $mv->name }}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            </div>
                            <span id="module"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                    </button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="save_and_update">
                                        Save & Add New
                                    </button>
                                    <a href="{{ route('admin.companyTeamList',$uuid) }}" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
