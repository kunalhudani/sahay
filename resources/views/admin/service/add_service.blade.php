@extends('layouts.admin')
@section('title','Add Service')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Add Service</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.serviceList') }}">Service List</a></li>
                            <li class="breadcrumb-item active">Add Service</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="card">
                        <div class="card-body">
                            <form class="custom-validation" action="{{ route('admin.saveService') }}" method="post" id="serviceForm" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Service Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="service_name" placeholder="Service Name" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label>HSN</label>
                                <input type="text" class="form-control" name="hsn" placeholder="Enter HSN" autocomplete="off"/>
                            </div><br>

                            <u><h5>Domestic Taxation</h5></u><br>

                            <div class="form-group">
                                <label>SGST<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number sgst" name="sgst" placeholder="Enter SGST" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label>CGST<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number cgst" name="cgst" placeholder="Enter CGST" autocomplete="off" required/>
                            </div>

                            <div class="form-group">
                                <label>GST</label>
                                <input type="text" class="form-control number gst" name="gst" placeholder="Enter GST" autocomplete="off" readonly="readonly"/>
                            </div>

                            <div class="form-group">
                                <label>IGST<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number" name="icgst" placeholder="Enter IGST" autocomplete="off" required/>
                            </div><br>

                            <u><h5>International Taxation</h5></u><br>

                            <div class="form-group">
                                <label class="control-label">Taxation Type<span class="mandatory">*</span></label>
                                <select class="form-control taxation_type" name="taxation_type" required>
                                    <option value="">Select Taxation Type</option>
                                    <option value="1">Non Taxable</option>
                                    <option value="2">Taxable</option>
                                </select>
                            </div>

                            <div class="form-group tax_taxation" style="display: none;">
                                <label>Tax<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number tax" name="tax" data-msg="Enter Tax" placeholder="Enter Tax" autocomplete="off"/>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="btn_submit" value="save">
                                        Save
                                </button>
                                <button type="submit" class="btn btn-danger waves-effect waves-light mr-1" name="btn_submit" value="save_and_update">
                                    Save & Add New
                                </button>
                                <a href="{{ route('admin.serviceList') }}" class="btn btn-secondary waves-effect">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
<script>

</script>
@endsection