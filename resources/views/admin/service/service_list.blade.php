@extends('layouts.admin')
@section('title','All Service')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">All Service</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">All Service</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Service</th>
                                    <th>HSN</th>
                                    <th>SGST</th>  
                                    <th>CGST</th> 
                                    <th>GST</th> 
                                    <th>IGST</th> 
                                    <th>International Tax</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($get_services))
                                @foreach($get_services as $gk => $gv)
                                    <tr>

                                        <td>{{ $gv->service_name }}</td>
                                        <td>{{ $gv->hsn }}</td>
                                        <td>{{ $gv->sgst }}</td>
                                        <td>{{ $gv->cgst }}</td>
                                        <td>{{ $gv->gst }}</td>
                                        <td>{{ $gv->icgst }}</td>
                                        @if($gv->tax != '')
                                            <td>{{ $gv->tax }}</td>
                                        @else
                                            <td>Non Taxable</td>
                                        @endif
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editService',$gv->id) }}" title="Edit Service" role="button"><i class="bx bx-pencil"></i></a>
                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteService',$gv->id) }}" title="Delete Service" role="button" onclick="return confirm('Do you want to delete this service?');"><i class="bx bx-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
@endsection