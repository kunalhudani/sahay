@extends('layouts.admin')
@section('title','Consultant List')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Consultant List</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Consultant List</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Full Name</th>
                                    <th>Mobile Number</th>
                                    <th>DOB</th>
                                    <th>Aadhar Card</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!is_null($get_consultants))
                                @foreach($get_consultants as $gk => $gv)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $gv->name }}</td>
                                        <td>{{ $gv->mobile }}</td>
                                        <td>{{ date('d/m/Y',strtotime($gv->dob)) }}</td>
                                        <td>{{ $gv->aadhar_card_number }}</td>
                                        <td>
                                            <a class="btn btn-primary waves-effect waves-light" href="{{ route('admin.editConsultants',$gv->id) }}" role="button"><i class="bx bx-pencil"></i></a>
                                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('admin.deleteConsultants',$gv->id) }}" role="button" onclick="return confirm('Do you want to delete this consultant?');"><i class="bx bx-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
@endsection