@extends('layouts.admin')
@section('title','Edit Consultant')
@section('content')
<style type="text/css">
#upload-demo{
    width: 250px;
    height: 250px;
    padding-bottom:25px;
}
</style>
<div class="page-content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Edit Consultant</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Edit Consultant</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>     
        <!-- end page title -->
        <!-- end row -->
        <form class="custom-validation" action="{{ route('admin.saveEditedConsultants') }}" method="post" id="editconsultantForm" enctype="multipart/form-data">
            @csrf
            <div class="row">

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            
                            <input type="hidden" name="id" id="consultant_id" value="{{ $find_consultant->id }}">
                            <input type="hidden" name="uuid" value="{{ $find_consultant->uuid }}">

                            <div class="form-group">
                                <label>Profile Image</label>
                                <center>
                                    <img class="rounded-circle avatar-xl" alt="200x200" width="200" @if($find_consultant->profile_image != '') src="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/profile_image/{{ $find_consultant->profile_image }}" @else src="{{ asset('images/users/user.png') }}" @endif data-holder-rendered="true" id="item-img-output">
                                    <input type="file" name="profile" class="item-img" id="my_file" style="display: none;" />
                                    <input type="hidden" name="profile_image" id="profile_image" value="">
                                    <figure style="margin-top:-25px;margin-right:-65px;">
                                        <figcaption><i class="fa fa-camera" style="color:white"></i></figcaption>
                                    </figure>
                                </center>
                            </div>

                            <div class="form-group">
                                <label>Full Name<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="full_name" placeholder="Full Name" autocomplete="off" value="{{ $find_consultant->name }}" required/>
                            </div>

                            <div class="form-group">
                                <label>DOB</label>
                                <input type="text" class="form-control" name="dob" placeholder="dd/mm/yyyy" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd/mm/yyyy" @if($findEmployee->dob != '')  value="{{ date('d/m/y',strtotime($find_consultant->dob)) }}" @endif data-date-end-date="0d">
                            </div>

                            <div class="form-group">
                                <label>Address<span class="mandatory">*</span></label>
                                <textarea class="form-control" name="address" id="address" placeholder="Address" autocomplete="off" required>{{ $find_consultant->address }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Mobile Number<span class="mandatory">*</span></label>
                                <input type="text" class="form-control number" name="mobile_number" placeholder="Mobile Number" autocomplete="off" maxlength="10" minlength="10" value="{{ $find_consultant->mobile }}"required/>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Status</label>
                                <select class="form-control" name="status" required>
                                    <option value="">Select Status</option>
                                    <option value="1" @if($find_consultant->status == 1) selected @endif>Active</option>
                                    <option value="2" @if($find_consultant->status == 2) selected @endif>Sabbatical</option>
                                    <option value="3" @if($find_consultant->status == 3) selected @endif>Suspended</option>
                                    <option value="4" @if($find_consultant->status == 4) selected @endif>Former</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Location<span class="mandatory">*</span></label>
                                <select class="select2 form-control select2-multiple" multiple="multiple" name="location[]" data-placeholder="Select Location" required>
                                    @forelse ($get_city as $gc => $gv)
                                        <option value="{{$gv->id}}"  @if(in_array($gv->id,$find_consultant_location)) selected @endif>{{$gv->city_name}}</option>
                                    @empty
                                        <option>Do Data Found</option>
                                    @endforelse
                                </select>
                                <span id="location"></span>
                            </div>

                            <div class="form-group">
                                <label>Professional Email ID<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="professional_email_id" placeholder="Professional Email ID" autocomplete="off" value="{{$find_consultant->professional_email_id}}" required/ >
                            </div>

                            <div class="form-group">
                                <label>Personal Email ID<span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="personal_email_id" placeholder="Personal Email ID" autocomplete="off" value="{{$find_consultant->personal_email_id}}" required/>
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password" id="password"/>
                            </div>

                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" />
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            
                            <div class="form-group">
                                <label>Aadhar Card Number</label>
                                <input type="text" class="form-control number" name="aadharcard_nunber" placeholder="Aadhar Card Number" autocomplete="off" maxlength="12" minlength="12" value="{{ $find_consultant->aadhar_card_number }}"/>
                            </div>

                            <div class="form-group">
                                <label>PAN Number</label>
                                <input type="text" class="form-control" name="pancard_number" placeholder="PAN Number" autocomplete="off" maxlength="10" minlength="10" value="{{ $find_consultant->pan_number }}"/>
                            </div>

                            <div class="form-group">
                                <label>GSTN</label>
                                <input type="text" class="form-control alphanumeric" name="gstn_number" placeholder="GSTN" autocomplete="off" maxlength="15" minlength="15" value="{{$find_consultant->gstn_number}}" />
                            </div>

                            <div class="form-group">
                                <label>Aadhar Card Front Image Upload</label>
                                <input type="file" class="form-control dropify" name="aadharcard_image" placeholder="Mobile Number" autocomplete="off" data-default-file="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/aadharcard/{{ $find_consultant->aadhar_card_image }}"/>
                            </div>

                            <div class="form-group">
                                <label>Aadhar Card Back Image Upload</label>
                                <input type="file" class="form-control dropify" name="aadharcard_back_image" placeholder="Mobile Number" autocomplete="off" data-default-file="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/aadharcard/{{ $find_consultant->aadhar_card_back_image }}"/>
                            </div>
                            
                            <div class="form-group">
                                <label>PAN Card Image Upload</label>
                                <input type="file" class="form-control dropify" name="pancard_image" placeholder="Mobile Number" autocomplete="off" data-default-file="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/pancard_image/{{ $find_consultant->pan_card_image }}" />
                            </div>

                            <div class="form-group">
                                <label>Resume Upload (Image-PDF-Doc)</label>
                                <input type="file" class="form-control dropify" name="resume_upload" placeholder="Resume Upload (Image-PDF-Doc)" autocomplete="off" data-default-file="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/resume/{{ $find_consultant->resume_upload }}" />
                            </div>

                            <div class="form-group">
                                <label>MoU/Contract Upload</label>
                                <input type="file" class="form-control dropify" name="mou_upload" placeholder="MoU/Contract Upload" autocomplete="off" data-default-file="{{ env('BUCKET_URL') }}{{ $find_consultant->uuid }}/mou_upload/{{ $find_consultant->mou_upload }}" />
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                     <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Modules</h4>
                            <div class="row">
                            @if(!is_null($getModule))
                                @foreach($getModule as $mk => $mv)
                                    <div class="col-md-6">
                                        <div class="mt-4 mt-lg-0">
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" name="module[]" class="custom-control-input" id="customCheck{{$mk}}" value="{{ $mv->id }}" @if(in_array($mv->id,$getModuleId)) checked @endif>
                                                <label class="custom-control-label" for="customCheck{{$mk}}">{{ $mv->name }}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            </div>
                            <span id="module"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                        Update
                                    </button>
                                    <a href="{{ route('admin.consultantsList') }}" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
@endsection