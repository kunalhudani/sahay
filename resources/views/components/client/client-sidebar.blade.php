<div>
	<div class="vertical-menu">

	    <div data-simplebar class="h-100">

	        <!--- Sidemenu -->
	        <div id="sidebar-menu">
	            <!-- Left Menu Start -->
	            <ul class="metismenu list-unstyled" id="side-menu">
	                <li class="menu-title">Menu</li>

	                <li>
	                    <a href="{{ route('dashboard') }}" class="waves-effect">
	                        <i class="bx bx-home-circle"></i><span class="badge badge-pill badge-info float-right">3</span>
	                        <span>Dashboard</span>
	                    </a>
	                </li>
	            </ul>
	            
	        </div>
	        <!-- Sidebar -->
	    </div>
	</div>
</div>