<div>

	<div class="vertical-menu">

	    <div data-simplebar class="h-100">

	        <!--- Sidemenu -->
	        <div id="sidebar-menu">
	            <!-- Left Menu Start -->
	            <ul class="metismenu list-unstyled" id="side-menu">
	                <li class="menu-title">Menu</li>
	                <!-- Dashboard -->
	                <li>
	                    <a href="{{ route('admin.dashboard') }}" class="waves-effect">
	                        <i class="bx bx-pie-chart-alt-2"></i>
	                        <span>Dashboard</span>
	                    </a>
	                </li>
	                <!-- Dashboard -->
	                
	                <!-- Employees -->
	                @if(in_array('employee',$module))
		                <li>
		                    <a href="javascript: void(0);" class="has-arrow waves-effect">
		                        <i class="bx bx-user"></i>
		                        <span>Employees</span>
		                    </a>
		                    <ul class="sub-menu" aria-expanded="false">
		                        <li><a href="{{ route('admin.addEmployee') }}">Add Employees</a></li>
		                        <li><a href="{{ route('admin.employeeList') }}">All Employees</a></li>
		                    </ul>
		                </li>
		            @endif
	                <!-- Employees -->

	                <!--Department-->
	                @if(in_array('client-department',$module))
	                <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bx-briefcase-alt-2"></i>
	                        <span>Department</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addClientDepartment') }}">Add Department</a></li>
	                        <li><a href="{{ route('admin.clientDepartmentList') }}">All Department</a></li>
	                    </ul>
	                </li>
	                @endif
	                <!--end-->

	                <!--Industry-->
	                @if(in_array('industry-type',$module))
	                <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bxs-factory"></i>
	                        <span>Industry</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addIndustry') }}">Add Industry</a></li>
	                        <li><a href="{{ route('admin.industryList') }}">All Industry</a></li>
	                    </ul>
	                </li>
	                @endif
	                <!--end-->

	                <!--Consultant-->
	                @if(in_array('consultants',$module))
	                 <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bx-user-voice"></i>
	                        <span>Consultant</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addConsultants') }}">Add Consultant</a></li>
	                        <li><a href="{{ route('admin.consultantsList') }}">All Consultant</a></li>
	                    </ul>
	                </li>
	                @endif
	                <!--end-->
		            
		             <!--Task Status-->
	                @if(in_array('task-status',$module))
	                <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bx-task"></i>
	                        <span>Task Status</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addTaskStatus') }}">Add Task Status</a></li>
	                        <li><a href="{{ route('admin.taskStatusList') }}">All Task Status</a></li>
	                    </ul>
	                </li>
	                @endif
	                <!--end-->
	                @if(in_array('service',$module))
	                <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bx-user"></i>
	                        <span>Services</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addService') }}">Add Services</a></li>
	                        <li><a href="{{ route('admin.serviceList') }}">All Services</a></li>
	                    </ul>
	                </li>
	                @endif
	                @if(in_array('company',$module))
		                <li>
		                    <a href="javascript: void(0);" class="has-arrow waves-effect">
		                        <i class="bx bx-user"></i>
		                        <span>Companies</span>
		                    </a>
		                    <ul class="sub-menu" aria-expanded="false">
		                        <li><a href="{{ route('admin.addCompany') }}">Add Company</a></li>
		                        <li><a href="{{ route('admin.companyList') }}">All Companies</a></li>
		                        <li><a href="{{ route('admin.companyEmployeeList') }}">Company User</a></li>
		                    </ul>
		                </li>
		            @endif

		            <li>
	                    <a href="javascript: void(0);" class="has-arrow waves-effect">
	                        <i class="bx bx-user"></i>
	                        <span>Invoice Listing</span>
	                    </a>
	                    <ul class="sub-menu" aria-expanded="false">
	                        <li><a href="{{ route('admin.addInvoice') }}">Add Invoice</a></li>
	                        <li><a href="{{ route('admin.invoiceList') }}">All Invoice</a></li>
	                    </ul>
	                </li>
		            
	            </ul>
	        </div>
	        <!-- Sidebar -->
	    </div>
	</div>
</div>