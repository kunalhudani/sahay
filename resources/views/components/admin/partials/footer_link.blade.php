<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('plugins/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('plugins/node-waves/waves.min.js') }}"></script>

<!-- apexcharts -->
<script src="{{ asset('plugins/apexcharts/apexcharts.min.js') }}"></script>

<script src="{{ asset('js/pages/dashboard.init.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>

<!-- jQuery Validation-->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<!-- Login js-->
<script src="{{ asset('js/common_validation.js') }}"></script>

<script src="{{ asset('plugins/dropify/dist/js/dropify.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/pages/developer.js') }}"></script>

<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('js/pages/datatables.init.js') }}"></script>   

<script src="{{ asset('plugins/cropper/croppie.js') }}"></script>

<script src="{{ asset('plugins/cropper/scripts.js') }}"></script>

<script src="{{ asset('js/validation.js') }}"></script>

<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
	
<script src="{{asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>

<script src="{{asset('plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

<script src="{{asset('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="{{ asset('js/pages/form-advanced.init.js') }}"></script>

<script src="{{ asset('js/common.js') }}"></script>

<script src="{{ asset('js/company_user.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script src="{{asset('plugins/apexcharts/apexcharts.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.init.js')}}"></script>

@if(route::is('admin.addService') || route::is('admin.editService')) 
<script src="{{ asset('js/service_page.js' )}}"></script> 
@endif