<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> ©  Sahay Management Consultancy.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    Designed & Developed by Finlark Technologies
                </div>
            </div>
        </div>
    </div>
</footer>
