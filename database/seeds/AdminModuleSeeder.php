<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_modules')->insert(
            [
                [
                    'admin_id' => '1',
                    'module_id' => '1',
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]
        );
    }
}
