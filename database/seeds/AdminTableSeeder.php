<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(
            [
                [
                    'name' => 'Master Admin',
                    'uuid' => '123',
                    'email' => 'admin@admins.com',
                    'password' => bcrypt('ihpl123adminlogin'),
                    'is_active' => true,
                    'address' => '408, Shivalik Abiase',
                    'mobile' => '1234567899',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
            ]
        );
    }
}
