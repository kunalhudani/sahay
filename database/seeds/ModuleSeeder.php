<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_modules')->insert(
            [
                [
                    'name' => 'Employee',
                    'module_id' => 'employee',
                    'slug' => 'employee',
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]
        );
    }
}
