<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Master User',
                    'email' => 'admin@users.com',
                    'password' => bcrypt('admin123'),
                    'is_active' => true,
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]
        );
    }
}
