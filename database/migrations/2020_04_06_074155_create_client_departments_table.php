<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_departments', function (Blueprint $table) {
            $table->id();
            $table->string('department_name');
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_delete')->default(0)->comment = '0 for not deleted 2 deleted';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_departments');
    }
}
