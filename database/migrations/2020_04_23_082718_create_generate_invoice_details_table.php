<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenerateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generate_invoice_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('payment_id');
            $table->bigInteger('invoice_id');
            $table->bigInteger('invoice_no')->nullable();
            $table->double('paid_amount',10, 2)->nullable();
            $table->double('tax_amount',10, 2)->nullable();
            $table->double('tds_amount',10, 2)->nullable();
            $table->bigInteger('client_id');
            $table->text('address');
            $table->string('gstn');
            $table->string('invoice_date');
            $table->string('due_date');
            $table->string('state_of_supply');
            $table->bigInteger('state_code');
            $table->integer('tax_type')->nullable()->comment = '1 for same state, 2 diffent state, 3 International';
            $table->double('total_sub_total',10, 2)->nullable();
            $table->double('total_tax_amount',10, 2)->nullable();
            $table->double('total_sgst',10, 2)->nullable();
            $table->double('total_cgst',10, 2)->nullable();
            $table->double('total_payable',10, 2)->nullable();
            $table->tinyInteger('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generate_invoice_details');
    }
}
