<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultants', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('name');
            $table->date('dob')->nullable();
            $table->string('address');
            $table->string('mobile');
            $table->string('status')->comment = '1 for Active 2 Sabbatical 3 Suspended 4 Former';
            $table->string('professional_email_id')->nullable();
            $table->string('personal_email_id')->nullable();
            $table->string('password');
            $table->string('profile_image')->nullable();
            $table->string('aadhar_card_number')->nullable();
            $table->string('pan_number')->nullable();
            $table->string('gstn_number')->nullable();
            $table->string('profile_photo')->nullable();
            $table->string('aadhar_card_image')->nullable();
            $table->string('aadhar_card_image_back_side')->nullable();
            $table->string('pan_card_image')->nullable();
            $table->string('resume_upload')->nullable();
            $table->string('mou_upload')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultants');
    }
}
