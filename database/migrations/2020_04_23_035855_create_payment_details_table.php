<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_id');
            $table->string('date_of_entery');
            $table->double('paid_amount',10,2)->nullable();
            $table->double('tax_amount',10,2)->nullable();
            $table->double('tds_deducted')->nullable();
            $table->double('tds_amount',10,2)->nullable();
            $table->tinyInteger('is_invoice_generated')->default(0);
            $table->tinyInteger('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}
