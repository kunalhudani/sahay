<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_services', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('invoice_id')->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('service_id')->nullable();
            $table->string('hsn')->nullable();
            $table->text('description')->nullable();
            $table->double('quantity',10, 2)->nullable();
            $table->double('rate',10, 2)->nullable();
            $table->double('subtotal',10, 2)->nullable();
            $table->double('tax',10, 2)->nullable();
            $table->double('total_tax_payment',10,2)->nullable();
            $table->double('total_sgst_payment',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_services');
    }
}
