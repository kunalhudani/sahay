<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('service_name');
            $table->string('hsn')->nullable();
            $table->double('sgst',10, 2);
            $table->double('cgst',10, 2);
            $table->double('gst',10, 2)->nullable();
            $table->double('icgst',10, 2);
            $table->integer('taxation_type')->comment = '1 Non Taxable  2 Taxable';
            $table->double('tax',10, 2)->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
