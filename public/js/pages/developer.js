$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on("input", ".number", function() {
    this.value = this.value.replace(/\D/g,'');  
});

$('.dropify').dropify();