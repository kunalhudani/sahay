$(document).ready(function() {

    //custom validation method
    $.validator.addMethod("customemail", 
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Please enter email id along with domain name."
    );

    // validate signup form on keyup and submit
    $("#addEmployee").validate({
        errorElement: 'span',
        rules: {
            full_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            email: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/employee/check-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#employee_id').val()
                        },
                    }
                }
            },
            password:{
                minlength: 6,
            },
            confirm_password:{
                minlength: 6,
                equalTo:"#password"  
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            
            if(element.attr("name") == 'module[]'){ 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            full_name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            email: {
                required: 'Enter email ID',
                email: 'Enter valid email ID',
                remote:'Email id already exists'
            },
            password:{
               minlength:"Enter minimum 6 character"
            },
            confirm_password:{
                equalTo:"The new passwords do not match. Please check again"
            },
            'module[]':{
                required: 'Select atleast one module from list',
            }
        }
    });

    $("#editEmployee").validate({
        errorElement: 'span',
        rules: {
            full_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            email: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/employee/check-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#employee_id').val()
                        },
                    }
                }
            },
            password:{
                minlength: 6,
            },
            confirm_password:{
                minlength: 6,
                equalTo:"#password"  
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            
            if(element.attr("name") == 'module[]'){ 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            full_name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            email: {
                required: 'Enter email ID',
                email: 'Enter valid email ID',
                remote:'Email id already exists'
            },
            password:{
               required:"Enter password",
               minlength:"Enter minimum 6 character"
            },
            confirm_password:{
                required:"Enter confirm password",
                equalTo:"The new passwords do not match. Please check again"
            },
            'module[]':{
                required: 'Select atleast one module from list',
            }
        }
    });

    //add and edit client department validation
    $("#clientDepartment").validate({
        errorElement: 'span',
        rules: {
            department_name: {
                required: true,
                remote:{
                    url: "/administrator-panel/client-department/check-department-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            }
        },
        messages: {
            department_name: {
                required: 'Enter department name',
                remote:'Department already exists'
            }
        }
    });

    //add and edit industry form validation
    $("#industryForm").validate({
        errorElement: 'span',
        rules: {
            industry_name: {
                required: true,
                remote:{
                    url: "/administrator-panel/industry-type/check-industry-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            }
        },
        messages: {
            industry_name: {
                required: 'Enter industry name',
                remote:'Industry already exists'
            }
        }
    });

    //add consultant form validation
    $("#consultantForm").validate({
        errorElement: 'span',
        rules: {
            full_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            status: {
              required: true,  
            },
            'location[]': {
              required: true,  
            },
            professional_email_id: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/consultants/check-professional-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#consultant_id').val()
                        },
                    }
                }
            },

            personal_email_id: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/consultants/check-personal-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#consultant_id').val()
                        },
                    }
                }
            },
            password:{
                required: true,
                minlength: 6,
            },
            confirm_password:{
                required: true,
                minlength: 6,
                equalTo:"#password"  
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            
            if(element.attr("name") == 'location[]'){ 
                error.insertAfter('#location');
            } else if(element.attr("name") == 'module[]') { 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            full_name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            status: {
                required: 'Select status',
            },
            'location[]': {
                required: 'Select location',
            },
            professional_email_id: {
                required: 'Enter professional email ID',
                email: 'Enter valid email ID',
                remote:'Professional email ID already exists'
            },
            personal_email_id: {
                required: 'Enter personal email ID',
                email: 'Enter valid email ID',
                remote:'Personal email ID already exists'  
            },
            password:{
               required:"Enter password",
               minlength:"Enter minimum 6 character"
            },
            confirm_password:{
                required:"Enter confirm password",
                equalTo:"The new passwords do not match. Please check again"
            },
            'module[]':{
                required: "Select atleast one moudle from list",
            }
        }
    });

    //edit consultant form validation
    $("#editconsultantForm").validate({
        errorElement: 'span',
        rules: {
            full_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            status: {
              required: true,  
            },
            'location[]': {
              required: true,  
            },
            professional_email_id: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/employee/check-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#employee_id').val()
                        },
                    }
                }
            },
            personal_email_id: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/employee/check-email-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#employee_id').val()
                        },
                    }
                }
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            
            if(element.attr("name") == 'location[]'){ 
                error.insertAfter('#location');
            } else if(element.attr("name") == 'module[]') { 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            full_name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            status: {
                required: 'Select status',
            },
            'location[]': {
                required: 'Select location',
            },
            professional_email_id: {
                required: 'Enter professional email ID',
                email: 'Enter valid email ID',
                remote:'Email ID already exists'
            },
            personal_email_id: {
                required: 'Enter personal email ID',
                email: 'Enter valid email ID',
                remote:'Email ID already exists'  
            },
            'module[]':{
                required: "Select atleast one module from list",
            }
        }
    }); 

    //add and edit task status form validation
    $("#taskStatusForm").validate({
        errorElement: 'span',
        rules: {
            task_status: {
                required: true,
                remote:{
                    url: "/administrator-panel/task-status/check-task-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            }
        },
        messages: {
            task_status: {
                required: 'Enter task status',
                remote:'Task status already exists'
            }
        }
    });

    //add company form
    $("#companyForm").validate({
        errorElement: 'span',
        rules: {
            company_logo: {
                required: true,
            },
            company_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            'location[]': {
              required: true,  
            },
            industry_type: {
                required: true
            },
            'engagement[]': {
              required: true,  
            },
            days:{
                required: true,
            },
            company_state: {
                required: true,
            },
            company_country: {
                required: true,
            },
            'employees[]': {
              required: true,  
            },
            'consultant[]': {
              required: true,  
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == 'location[]'){ 
                error.insertAfter('#location');
            } else if(element.attr("name") == 'engagement[]'){
                error.insertAfter('#engagement');
            } else if(element.attr("name") == 'employees[]'){
                error.insertAfter('#employees');
            } else if(element.attr("name") == 'consultant[]'){
                error.insertAfter('#consultant');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            company_logo: {
                required: 'Enter company logo',
            },
            company_name: {
                required: 'Entry company name',
            },
            address: {
                required: 'Entry address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            'location[]': {
                required: 'Select location',
            },
            industry_type: {
                required: 'Select Industry Type'
            },
            'engagement[]': {
              required: 'Select engagement type',  
            },
            days:{
                required: 'Enter no of days per month',
            },
            company_state:{
                required: 'Enter state',
            },
            company_country:{
                required: 'Enter country',
            },
            'employees[]': {
              required: 'Select employees',  
            },
            'consultant[]': {
              required: 'Select consultant',  
            }
        }
    });

    //edit company form
    $("#editCompanyForm").validate({
        errorElement: 'span',
        rules: {
            company_name: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            'location[]': {
              required: true,  
            },
            industry_type: {
                required: true
            },
            'engagement[]': {
              required: true,  
            },
            days:{
                required: true,
            },
            company_state: {
                required: true,
            },
            company_country: {
                required: true,
            },
            'employees[]': {
              required: true,  
            },
            'consultant[]': {
              required: true,  
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == 'location[]'){ 
                error.insertAfter('#location');
            } else if(element.attr("name") == 'engagement[]'){
                error.insertAfter('#engagement');
            } else if(element.attr("name") == 'employees[]'){
                error.insertAfter('#employees');
            } else if(element.attr("name") == 'consultant[]'){
                error.insertAfter('#consultant');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            company_name: {
                required: 'Entry company name',
            },
            address: {
                required: 'Entry address',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            'location[]': {
                required: 'Select location',
            },
            industry_type: {
                required: 'Select Industry Type'
            },
            'engagement[]': {
              required: 'Select engagement type',  
            },
            days:{
                required: 'Enter no of days per month',
            },
            company_state:{
                required: 'Enter state',
            },
            company_country:{
                required: 'Enter country',
            },
            'employees[]': {
              required: 'Select employees',  
            },
            'consultant[]': {
              required: 'Select consultant',  
            }
        }
    });

    $("#companyUserForm").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true,
            },
            user_type: {
                required: true,
            },
            department: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                customemail : true,
            },
            password:{
                required: true,
                minlength: 6,
            },
            confirm_password:{
                required: true,
                minlength: 6,
                equalTo:"#password"  
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == 'module[]'){ 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            user_type:{
                required: 'Select user type',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            email: {
                required: 'Enter email ID',
                email: 'Enter valid email ID',
                remote:'Email id already exists'
            },
            password:{
               required:"Enter password",
               minlength:"Enter minimum 6 character"
            },
            confirm_password:{
                required:"Enter confirm password",
                equalTo:"The new passwords do not match. Please check again"
            },
            'module[]':{
                required: "Select atleast one moudle from list",
            }
        }
    });

    $("#editCompanyUserForm").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true,
            },
            user_type: {
                required: true,
            },
            department: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                customemail : true,
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            'module[]':{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == 'module[]'){ 
                error.insertAfter('#module');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            name: {
                required: 'Enter full name',
            },
            address: {
                required: 'Enter address',
            },
            user_type:{
                required: 'Select user type',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            email: {
                required: 'Enter email ID',
                email: 'Enter valid email ID',
                remote:'Email id already exists'
            },
            'module[]':{
                required: "Select atleast one moudle from list",
            }
        }
    });

    $("#editCompanyEmployee").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                customemail : true,
                remote:{
                    url: "/administrator-panel/company/check-employee-email",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            },
            mobile_number: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            password:{
                minlength: 6,
            },
            confirm_password:{
                minlength: 6,
                equalTo:"#password"  
            }
        },
        messages: {
            name: {
                required: 'Enter full name',
            },
            mobile_number: {
                required: 'Enter mobile number',
                minlength:"Enter minimum 10 digit mobile number",
                maxlength:"Enter minimum 10 digit mobile number",
            },
            email: {
                required: 'Enter email ID',
                email: 'Enter valid email ID',
                remote:'Email id already exists'
            },
            password:{
               required:"Enter password",
               minlength:"Enter minimum 6 character"
            },
            confirm_password:{
                required:"Enter confirm password",
                equalTo:"The new passwords do not match. Please check again"
            }
        }
    });
    
    $("#companyProfile").validate({
        errorElement: 'span',
        rules: {
            company_name: {
                required: true,
            },
            company_address: {
                required: true,
            },
            company_state: {
                required: true,
            },
            company_country: {
                required: true,
            }
        },
        messages: {
            company_name: {
                required: 'Enter company name',
            },
            company_address: {
                required: 'Enter company address',
            },
            company_state: {
                required: 'Enter state',
            },
            company_country: {
                required: 'Enter country',
            }
        }
    });

    $("#serviceForm").validate({
        errorElement: 'span',
        rules: {
            service_name: {
                required: true,
                remote:{
                    url: "/administrator-panel/service/check-service-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            },
            hsn:{
                remote:{
                    url: "/administrator-panel/service/check-hsn-exists",
                    type: "post",
                    data:{
                        id : function() {
                            return $('#id').val()
                        },
                    }
                }
            },
            sgst: {
                required: true,
            },
            cgst: {
                required: true,
            },
            icgst: {
                required: true,
            },
            taxation_type: {
                required: true,
            }
        },
        messages: {
            service_name: {
                required: 'Enter service name',
                remote:'Service name already exists'
            },
            hsn: {
                remote:'HSN already exists'
            },
            sgst: {
                required: 'Enter SGST',
            },
            cgst: {
                required: 'Enter CGST',
            },
            icgst: {
                required: 'Enter IGST',
            },
            taxation_type: {
                required: 'Select Taxation Type',
            }
        }
    });
    
    $("#saveInvoice").validate({
        errorElement: 'span',
        rules: {
            client_name: {
                required: true,
            },
            address: {
                required: true,
            },
            'consultant[]': {
                required: true,
            },
            invoice_date:{
                required: true,
            },
            due_date:{
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            
            if(element.attr("name") == 'consultant[]'){ 
                error.insertAfter('#consultant');
            } else { 
                error.insertAfter( element );
            }
        },
        messages: {
            client_name: {
                required: 'Enter select client'
            },
            address: {
                required: 'Enter Address',
            },
            'consultant[]': {
                required: 'Select consultant',
            },
            invoice_date: {
                required: 'Select invoice date',
            },
            due_date: {
                required: 'Select due date',
            }
        }
    });
    $("#paymentForm").validate({
        errorElement: 'span',
        rules: {
            date_of_entery: {
                required: true,
            },
            paid_amount: {
                required: true,
            },
            tax_amount: {
                required: true,
            },
            tds_deducted: {
                required: true,
            }
        },
        messages: {
            date_of_entery: {
                required: 'Select Date Of Entry',
            },
            paid_amount: {
                required: 'Enter Paid Amount',
            },
            tax_amount: {
                required: 'Enter Tax Amount',
            },
            tds_deducted: {
                required: 'Enter Tax Deducted',
            }
        }
    });
});

