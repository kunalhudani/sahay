<?php

Route::get('/', 'HomeController@index')->name('index');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'client'], function () {

	Route::get('/dashboard', 'HomeController@clientDashboard')->name('dashboard');
	//image crop
	Route::post('/crop-image', 'HomeController@cropImage')->name('cropImage');
	
});
