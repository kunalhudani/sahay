<?php

	// Authentication admin Login Routes
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\LoginController@login')->name('admin.postlogin');
	Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

	//forget and reset password
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.auth.password.reset');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.passwordemail');
	Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('admin.auth.password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.resetpassword');

	//Dashboard Route....
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('admin-profile', 'AdminController@adminProfile')->name('admin.profile');
	Route::post('admin-update-profile', 'AdminController@adminProfileUpdate')->name('admin.update.profile');

	//Change Password
	Route::get('/change-password', 'AdminController@changeAdminPassword')->name('admin.changePassoword');
	Route::post('/update-password', 'AdminController@updateAdminPassword')->name('admin.updatePassword');

	//common user for admin
	Route::post('/get-state', 'AdminController@getAutoSuggestState')->name('getAutoSuggestState');
	Route::post('/get-country', 'AdminController@getAutoSuggestCountry')->name('getAutoSuggestCountry');
	Route::post('/get-clients', 'AdminController@getAutoSuggestClient')->name('getAutoSuggestClient');

	//company profile
	Route::match(['get', 'post'],'company-profile', 'AdminController@companyProfile')->name('admin.companyProfile');


	//Employee Route
	Route::get('/employee/employee-list', 'EmployeeController@employeeList')->name('admin.employeeList');
	Route::get('/employee/add-employee', 'EmployeeController@addEmployee')->name('admin.addEmployee');
	Route::post('/employee/save-employee', 'EmployeeController@saveEmployee')->name('admin.saveEmployee');
	Route::get('/employee/edit-employee/{id}', 'EmployeeController@editEmployee')->name('admin.editEmployee');
	Route::post('/employee/save-edited-employee', 'EmployeeController@saveEditedEmployee')->name('admin.saveEditedEmployee');
	Route::get('/employee/delete-employee/{id}', 'EmployeeController@deleteEmployee')->name('admin.deleteEmployee');

	/*Route::post('/employee/crop-image', 'EmployeeController@cropImage')->name('admin.cropImage');*/

	Route::post('/employee/check-email-exists', 'EmployeeController@checkEmailExists')->name('admin.checkEmailExists');

	//client department
	Route::get('/client-department/department-list', 'ClientDepartmentController@clientDepartmentList')->name('admin.clientDepartmentList');
	Route::get('/client-department/add-department', 'ClientDepartmentController@addClientDepartment')->name('admin.addClientDepartment');
	Route::post('/client-department/save-department', 'ClientDepartmentController@saveClientDepartment')->name('admin.saveClientDepartment');
	Route::get('/client-department/edit-department/{id}', 'ClientDepartmentController@editClientDepartment')->name('admin.editClientDepartment');
	Route::post('/client-department/save-edited-department', 'ClientDepartmentController@saveEditedClientDepartment')->name('admin.saveEditedClientDepartment');
	Route::get('/client-department/delete-department/{id}', 'ClientDepartmentController@deleteClientDepartment')->name('admin.deleteClientDepartment');

	//check department already exists
	Route::post('/client-department/check-department-exists', 'ClientDepartmentController@checkDepartmentExists')->name('admin.checkDepartmentExists');

	//industry
	Route::get('/industry-type/industry-list', 'IndustryController@industryList')->name('admin.industryList');
	Route::get('/industry-type/add-industry', 'IndustryController@addIndustry')->name('admin.addIndustry');
	Route::post('/industry-type/save-industry', 'IndustryController@saveIndustry')->name('admin.saveIndustry');
	Route::get('/industry-type/edit-industry/{id}', 'IndustryController@editIndustry')->name('admin.editIndustry');
	Route::post('/industry-type/save-edited-industry', 'IndustryController@saveEditedIndustry')->name('admin.saveEditedIndustry');
	Route::get('/industry-type/delete-industry/{id}', 'IndustryController@deleteIndustry')->name('admin.deleteIndustry');

	//check industry already exists
	Route::post('/industry-type/check-industry-exists', 'IndustryController@checkIndustryExists')->name('admin.checkIndustryExists');


	//consultonts
	Route::get('/consultants/consultants-list', 'ConsultantController@consultantsList')->name('admin.consultantsList');
	Route::get('/consultants/add-consultants', 'ConsultantController@addConsultants')->name('admin.addConsultants');
	Route::post('/consultants/save-consultants', 'ConsultantController@saveConsultants')->name('admin.saveConsultants');
	Route::get('/consultants/edit-consultants/{id}', 'ConsultantController@editConsultants')->name('admin.editConsultants');
	Route::post('/consultants/save-edited-consultants', 'ConsultantController@saveEditedConsultants')->name('admin.saveEditedConsultants');
	Route::get('/consultants/delete-consultants/{id}', 'ConsultantController@deleteConsultants')->name('admin.deleteConsultants');
	
	Route::post('/consultants/check-professional-email-exists', 'ConsultantController@checkProfessionalEmailExists')->name('admin.checkProfessionalEmailExists');
	Route::post('/consultants/check-personal-email-exists', 'ConsultantController@checkPersonalEmailExists')->name('admin.checkPersonalEmailExists');

	
	//task status
	Route::get('/task-status/task-list', 'TaskStatusController@taskStatusList')->name('admin.taskStatusList');
	Route::get('/task-status/add-task', 'TaskStatusController@addTaskStatus')->name('admin.addTaskStatus');
	Route::post('/task-status/save-task', 'TaskStatusController@saveTaskStatus')->name('admin.saveTaskStatus');
	Route::get('/task-status/edit-task/{id}', 'TaskStatusController@editTaskStatus')->name('admin.editTaskStatus');
	Route::post('/task-status/save-edited-task', 'TaskStatusController@saveEditedTaskStatus')->name('admin.saveEditedTaskStatus');
	Route::get('/task-status/delete-task/{id}', 'TaskStatusController@deleteTaskStatus')->name('admin.deleteTaskStatus');

	//check task status already exists
	Route::post('/task-status/check-task-exists', 'TaskStatusController@checkTaskExists')->name('admin.checkTaskExists');


	//Company Route
	Route::get('/company/company-list', 'CompanyController@companyList')->name('admin.companyList');
	Route::get('/company/add-company', 'CompanyController@addCompany')->name('admin.addCompany');
	Route::post('/company/save-company', 'CompanyController@saveCompany')->name('admin.saveCompany');
	Route::get('/company/edit-company/{id}', 'CompanyController@editCompany')->name('admin.editCompany');
	Route::post('/company/save-edited-company', 'CompanyController@saveEditedCompany')->name('admin.saveEditedCompany');
	Route::get('/company/delete-company/{id}', 'CompanyController@deleteCompany')->name('admin.deleteCompany');

	Route::get('/company/company-team-list/{uuid}', 'CompanyController@companyTeamList')->name('admin.companyTeamList');
	Route::get('/company/add-company-user/{uuid}', 'CompanyController@addCompanyUser')->name('admin.addCompanyUser');
	Route::post('/company/save-company-user', 'CompanyController@saveCompanyUser')->name('admin.saveCompanyUser');
	Route::get('/company/edit-company-user/{uuid}/{id}', 'CompanyController@editCompanyUser')->name('admin.editCompanyUser');
	Route::post('/company/save-edited-company-user', 'CompanyController@saveEditedCompanyUser')->name('admin.saveEditedCompanyUser');
	Route::get('/company/delete-company-user/{uuid}/{id}', 'CompanyController@deleteCompanyUser')->name('admin.deleteCompanyUser');
	
	//
	Route::get('/company/employee-list', 'CompanyController@companyEmployeeList')->name('admin.companyEmployeeList');
	Route::get('/company/edit-company-employee/{id}', 'CompanyController@editCompanyEmployee')->name('admin.editCompanyEmployee');
	Route::get('/company/delete-company-employee/{id}', 'CompanyController@deleteCompanyEmployee')->name('admin.deleteCompanyEmployee');
	Route::post('/company/save-edited-company-employee', 'CompanyController@saveEditedCompanyEmployee')->name('admin.saveEditedCompanyEmployee');
	Route::post('/company/check-employee-email', 'CompanyController@checkEmployeeEmail')->name('admin.checkEmployeeEmail');

	Route::post('/company/get-employee-suggestion', 'CompanyController@getEmployeeSuggestion')->name('admin.getEmployeeSuggestion');
	Route::post('/company/employee-details', 'CompanyController@employeeDetails')->name('admin.employeeDetails');
	

	//Company Route
	Route::get('/service/service-list', 'ServiceController@serviceList')->name('admin.serviceList');
	Route::get('/service/add-service', 'ServiceController@addService')->name('admin.addService');
	Route::post('/service/save-service', 'ServiceController@saveService')->name('admin.saveService');
	Route::get('/service/edit-service/{id}', 'ServiceController@editService')->name('admin.editService');
	Route::post('/service/save-edited-service', 'ServiceController@saveEditedService')->name('admin.saveEditedService');
	Route::get('/service/delete-service/{id}', 'ServiceController@deleteService')->name('admin.deleteService');
	Route::post('/service/check-service-exists', 'ServiceController@checkService')->name('admin.checkService');


	//invoice routes
	Route::get('/invoice/invoice-list', 'InvoiceController@invoiceList')->name('admin.invoiceList');
	Route::post('/invoice/get-company-details', 'InvoiceController@getCompanyDetails')->name('admin.getCompanyDetails');
	Route::get('/invoice/add-invoice', 'InvoiceController@addInvoice')->name('admin.addInvoice');
	Route::post('/invoice/save-invoice', 'InvoiceController@saveInvoice')->name('admin.saveInvoice');
	Route::get('/invoice/edit-invoice/{id}', 'InvoiceController@editInvoice')->name('admin.editInvoice');
	Route::post('/invoice/save-edited-invoice', 'InvoiceController@saveEditedInvoice')->name('admin.saveEditedInvoice');
	Route::post('/invoice/send-client-reminder', 'InvoiceController@sendClientReminder')->name('admin.sendClientReminder');
	Route::get('/invoice/send-performa/{id}', 'InvoiceController@sendPerforma')->name('admin.sendPerforma');
	Route::post('/invoice/save-expected', 'InvoiceController@saveExpected')->name('admin.saveExpected');
	Route::get('/invoice/delete-invoice/{id}', 'InvoiceController@deleteInvoice')->name('admin.deleteInvoice');

	//payment routes
	Route::get('/invoice/payment-detail/{id}', 'InvoiceController@paymentDetail')->name('admin.paymentDetail');
	Route::post('/invoice/save-payment-detail', 'InvoiceController@savePaymentDetail')->name('admin.savePaymentDetail');
	Route::post('/invoice/update-payment-detail', 'InvoiceController@saveEditedPaymentDetail')->name('admin.saveEditedPaymentDetail');
	Route::get('/invoice/deleted-payment-detail/{id}/{invoice_id?}', 'InvoiceController@deletePaymentDetail')->name('admin.deletePaymentDetail');

	Route::get('/invoice/genrate-invoice/{id}/{invoice_id?}', 'InvoiceController@generateInvoice')->name('admin.generateInvoice');
	Route::post('/invoice/save-genrate-invoice', 'InvoiceController@saveEditedGenerateInvoice')->name('admin.saveEditedGenerateInvoice');

	//genrated invoice route
	Route::get('/invoice/generated-invoice-list/{id}', 'InvoiceController@genratedInvoiceList')->name('admin.genratedInvoiceList');
	Route::get('/invoice/edit-generated-invoice/{id}/{invoice_id?}', 'InvoiceController@editGenratedInvoice')->name('admin.editGenratedInvoice');
	Route::post('/invoice/update-genrate-invoice', 'InvoiceController@updatedGeneratedInvoice')->name('admin.updatedGeneratedInvoice');
	Route::get('/invoice/deleted-generated-invoice/{id}/{invoice_id?}', 'InvoiceController@deleteGeneratedInvoice')->name('admin.deleteGeneratedInvoice');
	Route::get('/invoice/send-generated-invoice/{id}/{invoice_id?}', 'InvoiceController@sendGeneratedInvoice')->name('admin.sendGeneratedInvoice');

	//reminder log listing
	Route::get('/invoice/reminder-list/{id}', 'InvoiceController@reminderList')->name('admin.reminderList');

	//autosuggestion service for invoice
	Route::post('/invoice/get-service', 'InvoiceController@getAutoSuggestService')->name('getAutoSuggestService');

	