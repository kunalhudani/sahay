<?php

namespace App\View\Components;

use App\Model\AdminModule;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class Sidebar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {   

        return view('components.admin.sidebar',[
            'module' => $this->modules()
        ]);
    }

    public function modules(){

        $module = array();
        
        if (Auth::guard('admin')->user()) {

            $checkPermission = AdminModule::where('admin_id',Auth::guard('admin')->user()->id)->with(['module'])->get();

            if(!is_null($checkPermission)){
                foreach($checkPermission as $pk => $pv){
                    $module[] = $pv->module->slug;
                }
            }
        }

        return $module;
    }
}
