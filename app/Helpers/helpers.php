<?php

if (!function_exists('is_json')) {

    /**
     * @param string $string
     * @return boolean
     */
    function is_json(string $string)
    {
        json_decode($string);
        return json_last_error() == JSON_ERROR_NONE;
    }
}


if (!function_exists('to_json')) {

    /**
     * @param string|object|array|mixed $data
     * @return object
     */
    function to_json($data)
    {
        return json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }
}


if (!function_exists('to_array')) {

    /**
     * Undocumented function
     *
     * @param string|object|array|mixed $data
     * @return array
     */
    function to_array($data)
    {
        if (is_array($data)) {
            return $data;
        }

        if (is_string($data) && is_json($data)) {
            return json_decode($data, true);
        }

        return json_decode(to_json($data), true);
    }
}


if (!function_exists('response_json')) {

    /**
     * @param $data
     * @return void
     */
    function response_json($data)
    {
        @header('Content-Type: application/json');
        echo to_json($data);
        exit;
    }
}


if (!function_exists('response_success')) {

    /**
     * @param array $data
     * @param array $meta
     * @param integer $code
     * @return void
     */
    function response_success($data = [], $meta = [], $httpCode = 200)
    {
        http_response_code($httpCode);
        response_json([
            'http_code' => $httpCode,
            'data' => is_array($data) ? $data : [$data],
            'meta' => is_array($meta) ? $meta : [$meta]
        ]);
    }
}


if (!function_exists('response_error')) {

    /**
     * @param string $message
     * @param integer $httpCode
     * @param array $params
     * @param string $code
     * @return void
     */
    function response_error(string $message = "error", int $httpCode = 400, $params = [], string $code = null)
    {
        if (!$code) {
            switch ($httpCode) {
                case 400: $code = 'BAD_REQUEST'; break;
                case 401: $code = 'UNAUTHORIZED'; break;
                case 403: $code = 'UNAUTHENTICATED'; break;
                case 404: $code = 'ENTITY_NOT_FOUND'; break;
                case 413: $code = 'PAYLOAD_TOO_LARGE'; break;
                case 422: $code = 'FORMAT_DATA_INVALID'; break;
                case 429: $code = 'TOO_MANY_REQUESTS'; break;
                case 500: $code = 'ERROR_SERVER'; break;
            }
        }

        http_response_code($httpCode);
        response_json([
            'http_code' => $httpCode,
            'message' => $message,
            'code' => $code,
            'params' => $params
        ]);
    }
}
