<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;


/**
 * Class StripeService
 * @package App\Services
 */
class StripeService
{
    /**
     * StripeService constructor.
     */
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }


    /**
     * @param string $email
     * @param string $token
     * @return array
     */
    public function store(string $email, string $token = "")
    {
        try {
            return to_array(\Stripe\Customer::create([
                'email' => $email,
                'source' => $token,
                'description' => "Create at " . date('l jS \of F Y h:i:s A'),
            ]));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: store");
            Log::channel('stripe')->error("Email: $email");
            Log::channel('stripe')->error("Token: $token");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param array $options
     * @return array
     */
    public function getCustomers(array $options = [])
    {
        try {
            return to_array(\Stripe\Customer::all($options));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: getCustomers");
            Log::channel('stripe')->error("Options: " . to_json($options));
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $cus_id
     * @return array
     */
    public function getCard($cus_id)
    {
        try {
            $customer = to_array(\Stripe\Customer::retrieve($cus_id));
            $srcs = $customer['sources'];
            $defaultSrc = $customer['default_source'];

            if ($srcs['total_count'] > 0) {
                foreach ($srcs['data'] as $src) {
                    if ($defaultSrc == $src['id'] && 'card' && $src['object']) return $src;
                }
            }
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: getCard");
            Log::channel('stripe')->error("Customer: $cus_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
        }

        return [];
    }


    /**
     * @param string $cus_id
     * @param array $card
     * @return boolean
     */
    public function isValidCard($cus_id, $card = null)
    {
        $card = is_null($card) ? $this->getCard($cus_id) : $card;
        if (empty($card)) return false;
        $currentMonth = explode('-', date('Y-m'));

        return $card['exp_year'] * 12 + $card['exp_month'] > (int) $currentMonth[0] * 12 + (int) $currentMonth[1];
    }


    /**
     * @param array $planFormat
     * @param string $cus_id
     * @param array $metadata
     * @param boolean $init
     * @param int $trial_end
     * @return array
     */
    public function createSubscription(array $planFormat, string $cus_id, $metadata  = [], $init = false, int $trial_end = 0)
    {
        try {
            $data = [
                "customer" => $cus_id,
                "items" => [
                    [
                        "plan" => $planFormat['plan_id'],
                        "quantity" => $planFormat['quantity']
                    ]
                ],
                "metadata" => $metadata,
                "payment_behavior" => "allow_incomplete",
                // "expand" => ["latest_invoice.payment_intent"],
            ];

            if ($init) {
                if (isset($planFormat['coupon'])) {
                    $data["coupon"] = $planFormat['coupon'];
                }
                $data["trial_from_plan"] = true;
            }

            if ($trial_end && $trial_end >= time()) {
                $data["trial_end"] = $trial_end;
            }

            return to_array(\Stripe\Subscription::create($data));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: createSubscription");
            Log::channel('stripe')->error("Customer: $cus_id");
            Log::channel('stripe')->error("Plan format: " . to_json($planFormat));
            Log::channel('stripe')->error("Trial end: $trial_end");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $subcription_id
     * @return boolean
     */
    public function deleteDiscount(string $subcription_id)
    {
        try {
            $sub = \Stripe\Subscription::retrieve($subcription_id);
            $sub->deleteDiscount();

            return true;
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: deleteDiscount");
            Log::channel('stripe')->error("Subcription: $subcription_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return false;
        }
    }


    /**
     * @param string $subcription_id
     * @param bool $cancel
     * @return array
     */
    public function toggleAutoRenew(string $subcription_id, bool $cancel)
    {
        try {
            return to_array(\Stripe\Subscription::update(
                $subcription_id,
                ['cancel_at_period_end' => $cancel]
            ));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: toggleAutoRenew");
            Log::channel('stripe')->error("Subcription: $subcription_id");
            Log::channel('stripe')->error("Cancel: $cancel");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $subcription_id
     * @return boolean
     */
    public function cancelSubscription(string $subcription_id)
    {
        try {
            $sub = \Stripe\Subscription::retrieve($subcription_id);
            $sub->cancel();
            return true;
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: cancelSubscription");
            Log::channel('stripe')->error("Subcription: $subcription_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return false;
        }
    }


    /**
     * @param string $subcription_id
     * @return array
     */
    public function retrieveSubscription(string $subcription_id)
    {
        try {
            return to_array(\Stripe\Subscription::retrieve($subcription_id));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: retrieveSubscription");
            Log::channel('stripe')->error("Subcription: $subcription_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $product_id
     * @return array
     */
    public function retrieveProduct(string $product_id)
    {
        try {
            return to_array(\Stripe\Product::retrieve($product_id));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: retrieveProduct");
            Log::channel('stripe')->error("Subcription: $product_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $coupon_id
     * @return array
     */
    public function retrieveCoupon(string $coupon_id)
    {
        try {
            return to_array(\Stripe\Coupon::retrieve($coupon_id));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: retrieveCoupon");
            Log::channel('stripe')->error("Subcription: $coupon_id");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $chargeId
     * @return array
     */
    public function retrieveCharge(string $chargeId)
    {
        try {
            return to_array(\Stripe\Charge::retrieve($chargeId));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: retrieveCharge");
            Log::channel('stripe')->error("Charge: $chargeId");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param array $options
     * @return array
     */
    public function getCharges(array $options = [])
    {
        try {
            return to_array(\Stripe\Charge::all($options));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: getCharges");
            Log::channel('stripe')->error("Options: " . to_json($options));
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $subcription_id
     * @param string $plan_id
     * @param int $quantity
     * @param array $metadata
     * @return array
     */
    public function updatePlan(string $subcription_id, string $plan_id, int $quantity, array $metadata  = [])
    {
        try {
            $subscription = \Stripe\Subscription::retrieve($subcription_id);

            return to_array(\Stripe\Subscription::update($subcription_id, [
                'cancel_at_period_end' => false,
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'plan' => $plan_id,
                        'quantity' => $quantity
                    ],
                ],
                'metadata' => $metadata,
                'prorate' => false,
                'trial_end' => $subscription->current_period_end
            ]));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: updatePlan");
            Log::channel('stripe')->error("Subcription: $subcription_id");
            Log::channel('stripe')->error("Plan: $plan_id");
            Log::channel('stripe')->error("Quantity: $quantity");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * @param string $stripe_token
     * @param string $plan
     * @return array
     */
    public function createCharge($stripe_token, $plan)
    {
        $locationServ = new LocationService();
        $currency = $locationServ->getCurrencyName();
        $amount = null;
        $description = null;

        switch ($plan) {
            case 'PLAN_BASIC':
                $amount = 1937;
                $description = "LVH Single Season Pass";
                break;

            default:
                return [];
        }

        try {
            return to_array(\Stripe\Charge::create([
                "source" => $stripe_token,
                "amount" => $amount,
                "currency" => $currency,
                "description" => $description,
            ]));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: createCharge");
            Log::channel('stripe')->error("Stripe token: $stripe_token");
            Log::channel('stripe')->error("Amount: $amount");
            Log::channel('stripe')->error("Currency: $currency");
            Log::channel('stripe')->error("Description:  $description");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }


    /**
     * getClientSecret function
     *
     * @param array $subscription
     * @return array
     */
    public function getClientSecret(array $subscription = [])
    {

        try {
            if ($subscription['status'] == "incomplete" || $subscription['status'] == "past_due") {

                $invoice = \Stripe\Invoice::retrieve($subscription['latest_invoice']);
                $pIntent = \Stripe\PaymentIntent::retrieve($invoice->payment_intent);

                if ($pIntent->status == "requires_action") {
                    // 3D Secure required
                    return [
                        'key' => $pIntent->client_secret,
                        'type' => 'payment_intent'
                    ];
                }
            }

            // if ($subscription['pending_setup_intent']) {
            //     $setupIntent = \Stripe\SetupIntent::retrieve($subscription['pending_setup_intent']);

            //     if ($setupIntent->status == "requires_action") {
            //         // 3D Secure required
            //         return [
            //             'key' =>  $setupIntent->client_secret,
            //             'type' => 'setup_intent'
            //         ];
            //     }
            // }
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: getPaymentIntent");
            Log::channel('stripe')->error("Options: " . to_json($subscription));
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
        }

        return [];
    }


    /**
     * @param string $intentId
     * @param string $paymentMethod
     * @return bool
     */
    public function confirmSetupIntent(string $intentId, string $paymentMethod)
    {
        try {
            $intent = \Stripe\SetupIntent::retrieve($intentId);
            $intent->confirm([
                'payment_method' => $paymentMethod,
            ]);

            return true;
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: confirmSetupIntent");
            Log::channel('stripe')->error("Intent: $intentId");
            Log::channel('stripe')->error("Payment method: $paymentMethod");
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return false;
        }
    }


    /**
     * @param string $plan
     * @return array
     */
    public function formatPlan(string $plan)
    {
        $planName = '';
        switch ($plan) {
            case 'PLAN_WEEKLY':
                $planName = 'STRIPE_PLAN_WEEKLY';
                break;
            case 'PLAN_BASIC':
                $planName = 'STRIPE_PLAN_BASIC';
                break;
            case 'PLAN_BASIC_TRIAL':
                $planName = 'STRIPE_PLAN_BASIC_TRIAL';
                break;
        }
        $quantity = env($planName . "_QUANTITY");

        $location = new LocationService();
        $countryGroup = $location->getGroupCountry();

        if (!empty($countryGroup)) {
            $planName .= '_' . $countryGroup;
        }

        return [
            'plan_id' => env($planName),
            'quantity' => $quantity
        ];
    }


    /**
     * @param string $id
     * @return array
     */
    public function deleteCustomer($id)
    {
        try {
            return to_array(\Stripe\Customer::update(
                $id,
                [
                    'description' => "Delete at " . date('l jS \of F Y h:i:s A')
                ]
            ));
        } catch (\Exception $e) {
            Log::channel('stripe')->error("Function: deleteCustomer");
            Log::channel('stripe')->error("Id: " . $id);
            Log::channel('stripe')->error("Error: {$e->getMessage()}");
            Log::channel('stripe')->error("Stack: {$e}");
            Log::channel('stripe')->error("------------------------------------------------------------------------------------------");
            return [];
        }
    }
}
