<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Class FileUploaderService
 * @package App\Services
 */
class FileUploaderService
{
    //jpg|jpeg|png|gif|bmp|pdf|html|text
    const ALLOW_UPLOAD_FILE_TYPE = [
        'image/jpeg' => 1,
        'image/png' => 1,
        'image/gif' => 1,
        'image/bmp' => 1,
        'application/pdf' => 1,
        'text/html' => 1,
        'text/plain' => 1,
        'image/svg+xml' => 1,
        'image/x-icon' => 1,
        'image/svg' => 1,
    ];

    /**
     * @var string
     */
    private $targetDir;
    /**
     * @var integer
     */
    private $maxSize;

    /**
     * FileUploaderService constructor.
     */
    public function __construct()
    {
        $this->targetDir = "/";
        $this->maxSize = 10 * 1024 * 1024;
    }

    /**
     * @return string
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

    /**
     * @param string $targetDir
     */
    public function setTargetDir($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @return integer
     */
    public function getMaxSize()
    {
        return $this->maxSize;
    }

    /**
     * @param integer $maxSize
     */
    public function setMaxSize($maxSize)
    {
        $this->maxSize = $maxSize;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isAllowedType(string $type)
    {
        return isset(self::ALLOW_UPLOAD_FILE_TYPE[$type]);
    }

    /**
     * @param float $size
     * @return bool
     */
    public function isAllowedSize(float $size)
    {
        return $size <= $this->getMaxSize();
    }

    /**
     * @param UploadedFile $file
     * @param bool $keepName
     * @return string
     */
    public function upload(UploadedFile $file, $keepName = false)
    {
        if (!empty($this->targetDir)) {
            if ($this->isAllowedType($file->getMimeType()) && $this->isAllowedSize($file->getSize())) {
                if (!is_dir($this->getTargetDir())) mkdir($this->getTargetDir(), 0777, true);

                $fileName = (!$keepName ? date('ymdHis') . '_' : '') . $file->getClientOriginalName();
                $this->remove($fileName);
                $file->move($this->getTargetDir(), $fileName);

                return $fileName;
            }
        }
        return '';
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function remove($fileName)
    {
        if (!empty($this->targetDir)) {
            $file_path = $this->getTargetDir() . '/' . $fileName;
            if (file_exists($file_path)) {
                unlink($file_path);
                return true;
            }
        }
        return false;
    }

    /**
     * @param $url
     * @param $fileName
     * @return string
     */
    public function download($url, $fileName = '', $extension = 'png')
    {
        if (@getimagesize($url)) {
            if (!is_dir($this->getTargetDir())) mkdir($this->getTargetDir(), 0777, true);

            $content = file_get_contents($url);
            $fileName = empty($fileName) ? md5(uniqid()) : $fileName;
            $pathFile = $this->getTargetDir() . "/$fileName.$extension";
            $this->remove($pathFile);
            file_put_contents($pathFile, $content);

            return $fileName;
        }

        return '';
    }
}
