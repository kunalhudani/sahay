<?php

namespace App\Services;


/**
 * Class LocationService
 * @package App\Services
 */
class LocationService
{
    /**
     * @return array
     */
    public function getIpinfo()
    {
        $ip = $_SERVER['REMOTE_ADDR'];

        $url = "https://ipinfo.io/" . $ip . "/json?token=" . config('services.ipinfo.access_token');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return to_array(curl_exec($ch));
    }

    /**
     * @return string
     */
    public function getGroupCountry()
    {
        $resutl = '';

        $details = $this->getIpinfo();

        if (!isset($details['country'])) {
            $details['country'] = '';
        }

        switch ($details['country']) {
            case 'GB':
                $resutl = 'GBP';
                break;
            case 'BE':
            case 'CY':
            case 'EE':
            case 'FI':
            case 'GR':
            case 'IT':
            case 'LV':
            case 'LT':
            case 'LU':
            case 'MT':
            case 'MC':
            case 'PT':
            case 'SM':
            case 'SI':
            case 'PO':
            case 'SK':
            case 'FR':
            case 'AT':
            case 'ES':
            case 'DE':
            case 'NL':
            case 'IE':
            case 'VA':
                $resutl = 'EUR';
                break;
        }

        return $resutl;
    }

    /**
     * @return array
     */
    public function getCurrency()
    {
        $result = ['¢', '$'];

        switch ($this->getGroupCountry()) {
            case 'GBP':
                $result = ['p', '£'];
                break;
            case 'EUR':
                $result = ['c', '€'];
                break;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getCurrencyName()
    {
        $currency = "usd";
        switch ($this->getGroupCountry()) {
            case 'GBP':
                $currency = "gbp";
                break;
            case 'EUR':
                $currency = "eur";
                break;
        }

        return $currency;
    }
}
