<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;


/**
 * Class S3Service
 * @package App\Services
 */
class S3Service extends FileUploaderService
{
    /**
     * @param UploadedFile $file
     * @param bool $keepName
     * @return string
     */
    public function upload(UploadedFile $file, $keepName = false)
    {
        if ($this->isAllowedType($file->getMimeType()) && $this->isAllowedSize($file->getSize())) {
            $fileName = $this->getTargetDir() . '/' . (!$keepName ? date('ymdHis') . '_' : '') . $file->getClientOriginalName();
            Storage::disk('s3')->put($fileName, file_get_contents($file));

            return env('AWS_CLOUDFRONT') ? env('AWS_CLOUDFRONT') . '/' . $fileName : Storage::disk('s3')->url($fileName);
        }

        return '';
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function remove($fileName)
    {
        $filePath = $this->getTargetDir() . '/' . $fileName;
        if (Storage::disk('s3')->exists($filePath)) {
            Storage::disk('s3')->delete($filePath);

            return true;
        }

        return false;
    }
}
