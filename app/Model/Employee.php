<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function employeemodule(){
    	return $this->hasMany('App\Model\EmployeeModule','employee_id','id');
    }
}
