<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GenerateInvoiceProjectIncharge extends Model
{
    public function project_incharge_name(){
        return $this->hasOne('App\Model\Consultant','id','project_incharge');
    }
}
