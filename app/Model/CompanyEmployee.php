<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyEmployee extends Model
{
    public function employee(){
    	return $this->hasOne('App\Model\Employee','id','employee_id');
    }
}
