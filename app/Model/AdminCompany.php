<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminCompany extends Model
{
    public function state_name(){
        return $this->hasOne('App\Model\State','id','state');
    }
    public function country_detail(){
        return $this->hasOne('App\Model\Country','id','country');
    }
}
