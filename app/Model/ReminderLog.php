<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReminderLog extends Model
{
    public function company(){
        return $this->hasOne('App\Model\Company','id','company_id');
    }

    public function invoice(){
        return $this->hasOne('App\Model\invoice','id','invoice_id');
    }

    public function employee(){
    	return $this->hasOne('App\Model\Employee','id','employee_id');
    }
}
