<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    public function payment_amount(){
        return $this->hasOne('App\Model\GenerateInvoiceDetail','payment_id','id');
    }
}
