<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function location(){
    	return $this->hasMany('App\Model\CompanyLocation','company_id','id');
    }
    public function state(){
        return $this->hasOne('App\Model\State','id','state_id');
    }
    public function country(){
        return $this->hasOne('App\Model\Country','id','country_id');
    }
    public function engagement(){
    	return $this->hasMany('App\Model\CompanyEngagement','company_id','id');
    }
    public function employee(){
    	return $this->hasMany('App\Model\CompanyEmployee','company_id','id');	
    }
    public function linked_employees(){
        return $this->hasMany('App\Model\LinkedCompanyEmployee','company_id','id');   
    }
    public function linked_consultants(){
        return $this->hasMany('App\Model\LinkedCompanyConsultant','company_id','id');   
    }
}
