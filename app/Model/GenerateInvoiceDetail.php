<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GenerateInvoiceDetail extends Model
{
    public function company(){
        return $this->hasOne('App\Model\Company','id','client_id');
    }
    public function state(){
        return $this->hasOne('App\Model\State','id','state_of_supply');
    }
    public function consultant(){
    	return $this->hasMany('App\Model\GenerateInvoiceProjectIncharge','invoice_id','id');
    }
    public function service(){
    	return $this->hasMany('App\Model\GenerateInvoiceService','invoice_id','id');
    }
}
