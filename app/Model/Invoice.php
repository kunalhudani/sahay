<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	public function company(){
        return $this->hasOne('App\Model\Company','id','client_id');
    }
    public function state(){
        return $this->hasOne('App\Model\State','id','state_of_supply');
    }
    public function consultant(){
    	return $this->hasMany('App\Model\InvoiceProjectIncharge','invoice_id','id');
    }
    public function service(){
    	return $this->hasMany('App\Model\InvoiceService','invoice_id','id');
    }
    public function payment_detail(){
        return $this->hasMany('App\Model\PaymentDetail','invoice_id','id')->where('is_delete',0);
    }
}
