<?php

namespace App\Http\Controllers;

use Mail;
use App\Model\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GlobalController extends Controller
{
    //GENERATE UUID
    public function generateUUID(){
		return (string) Str::uuid();
    }

    //Convert Date Format
    public function convertDate($date){
    	$date = explode('/',$date);
    	return $date[2]."-".$date[1]."-".$date[0];
    }

    public function uploadBucket($image,$path,$uuid){
        $imagedata = $image;
        $getName = $imagedata->getClientOriginalName();
        $extension = $imagedata->getClientOriginalExtension(); 
        $fileName = $getName;
        $destinationPath = "uploads/".$uuid."/".$path."/".$fileName;
        $s3 = \Storage::disk('s3');
        $s3->put($destinationPath, file_get_contents($imagedata), 'public');
        return $fileName;
    }

    public function uploadProfile($uuid,$name){

        $pathx = public_path().'/uploads/'.$name;

        $imagedata = $pathx;
        $fileName = $name;
        $destinationPath = "uploads/".$uuid."/profile_image/".$fileName;
        $s3 = \Storage::disk('s3');
        $s3->put($destinationPath, file_get_contents($imagedata), 'public');
        //unlink($pathx);
        return 'true';
    }

    public function getCompanyId($uuid){
        $findComapny = Company::where('uuid',$uuid)->first();
        return $findComapny->id;
    }

    //mail function
    public function sendMail($templete,$mail_message,$sub,$email = null){
        
       $maildata = array('bodymessage' => $mail_message);

        Mail::send('mail.'.$templete, $maildata, function($msg) use ($email,$sub) {
           $msg->to($email, 'Site Admin');
           $msg->subject($sub);
        }); 
   }

}
