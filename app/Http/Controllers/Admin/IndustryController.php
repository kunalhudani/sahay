<?php

namespace App\Http\Controllers\Admin;

use App\Model\Industry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndustryController extends Controller{

	public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //industry type listing
    public function industryList(){

        $get_industry = Industry::where('is_delete',0)->get();

        return view('admin.industry.industry_list',compact('get_industry'));
    }

    //add industry type form
    public function addIndustry(){

        return view('admin.industry.add_industry');
    }

    //save industry type
    public function saveIndustry(Request $request){
    	
        $save_industry = new Industry;
        $save_industry->industry_name = $request->industry_name;
        $save_industry->save();

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addIndustry'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Industry',
                      'message' => 'Industry Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.industryList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Industry',
                      'message' => 'Industry Successfully Added',
                  ],
            ]); 
        }
    }

    //edit industry type form
    public function editIndustry($id){

        $find_industry= Industry::where('id',$id)->first();

        return view('admin.industry.edit_industry',compact('find_industry'));
    }

    //update industry type
    public function saveEditedIndustry(Request $request){
    	
        $save_industry = Industry::findOrFail($request->id);
        $save_industry->industry_name = $request->industry_name;
        $save_industry->save();

        return redirect(route('admin.industryList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Industry',
                  'message' => 'Industry Successfully Updated',
              ],
        ]); 
    }

    //remove industry type
    public function deleteIndustry($id){

        $delete_industry = Industry::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.industryList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Industry',
                  'message' => 'Industry Successfully Deleted',
              ],
        ]); 

    }
    
    //check industry type exists or not
    public function checkIndustryExists(Request $request){

        $query = Industry::query();
        $query->where('industry_name',$request->industry_name);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $industry_name = $query->first();

        if(!is_null($industry_name)){
            return 'false';
        } else {
            return 'true';
        }
    }
    
}
