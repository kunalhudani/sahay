<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
use App\Model\Admin;
use App\Model\AdminModule;
use App\Model\Module;
use Illuminate\Http\Request;

class EmployeeController extends GlobalController
{
    public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //Employee List
    public function employeeList(){

        $getEmployee = Admin::where('is_active',1)->where('is_delete',0)->where('id','!=',1)->get();

        return view('admin.employee.employee_list',compact('getEmployee'));
    }

    //Add Employee
    public function addEmployee(){

        $getModule = Module::all();

        return view('admin.employee.add_employee',compact('getModule'));
    }

    //Save Employee
    public function saveEmployee(Request $request){

        $uuid = $this->generateUUID();

        $saveEmployee = new Admin;
        $saveEmployee->uuid = $uuid;
        $saveEmployee->name = $request->full_name;
        if($request->dob != ''){
            $saveEmployee->dob = $this->convertDate($request->dob);
        }
        $saveEmployee->address = $request->address;
        $saveEmployee->mobile = $request->mobile_number;
        $saveEmployee->email = $request->email;
        $saveEmployee->password = bcrypt($request->password);
        $saveEmployee->aadhar_card_number = $request->aadharcard_nunber;
        $saveEmployee->pan_number = strtoupper($request->pancard_number);
        $this->uploadProfile($uuid,$request->profile_image);
        $saveEmployee->profile_image = $request->profile_image;
        if(isset($request->aadharcard_image)){
            $aadharcard_image = $this->uploadBucket($request->aadharcard_image,'aadharcard',$uuid);
            $saveEmployee->aadhar_card_image = $aadharcard_image;
        }
        if(isset($request->aadharcard_back_image)){
            $aadharcard_back_image = $this->uploadBucket($request->aadharcard_back_image,'aadharcard',$uuid);
            $saveEmployee->aadhar_card_back_image = $aadharcard_back_image;
        }
        if(isset($request->pancard_image)){
            $pancard_image = $this->uploadBucket($request->pancard_image,'pancard_image',$uuid);
            $saveEmployee->pan_card_image = $pancard_image;
        }
        $saveEmployee->save();

        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $module = new AdminModule;
                $module->admin_id = $saveEmployee->id;
                $module->module_id = $mv;
                $module->save();
            }
        }

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addEmployee'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Employee',
                      'message' => 'Employee Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.employeeList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Employee',
                      'message' => 'Employee Successfully Added',
                  ],
            ]); 
        }
    }

    //Edit Employee
    public function editEmployee($id){

        $getModule = Module::all();

        $findEmployee = Admin::where('id',$id)->first();

        $getModuleId = AdminModule::where('admin_id',$id)->pluck('module_id')->toArray();

        return view('admin.employee.edit_employee',compact('findEmployee','getModule','getModuleId'));
    }

    //Save Edited Module
    public function saveEditedEmployee(Request $request){

        $saveEmployee = Admin::findOrFail($request->id);
        $saveEmployee->name = $request->full_name;
        if($request->dob != ''){
            $saveEmployee->dob = $this->convertDate($request->dob);
        } else {
            $saveEmployee->dob = null;
        }
        $saveEmployee->address = $request->address;
        $saveEmployee->mobile = $request->mobile_number;
        $saveEmployee->email = $request->email;
        $saveEmployee->profile_image = $request->profile_image;
        if($request->password != ''){
            $saveEmployee->password = bcrypt($request->password);
        }
        $saveEmployee->aadhar_card_number = $request->aadharcard_nunber;
        $saveEmployee->pan_number = strtoupper($request->pancard_number);
        if($request->profile_image != ''){
            $this->uploadProfile($request->uuid,$request->profile_image);
            $saveEmployee->profile_image = $request->profile_image;
        }
        if(isset($request->aadharcard_image)){
            $aadharcard_image = $this->uploadBucket($request->aadharcard_image,'aadharcard',$request->uuid);
            $saveEmployee->aadhar_card_image = $aadharcard_image;
        }
        if(isset($request->aadharcard_back_image)){
            $aadharcard_back_image = $this->uploadBucket($request->aadharcard_back_image,'aadharcard',$request->uuid);
            $saveEmployee->aadhar_card_back_image = $aadharcard_back_image;
        }
        if(isset($request->pancard_image)){
            $pancard_image = $this->uploadBucket($request->pancard_image,'pancard_image',$request->uuid);
            $saveEmployee->pan_card_image = $pancard_image;
        }
        $saveEmployee->save();

        $removeModule = AdminModule::where('admin_id',$request->id)->delete();

        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $module = new AdminModule;
                $module->admin_id = $request->id;
                $module->module_id = $mv;
                $module->save();
            }
        }

        return redirect(route('admin.employeeList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Employee',
                  'message' => 'Employee Successfully Added',
              ],
        ]); 
    }

    //Delete Employee
    public function deleteEmployee($id){

        $deleteEmployee = Admin::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.employeeList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Employee',
                  'message' => 'Employee Successfully Deleted',
              ],
        ]); 

    }
    
    //Check Email Exists or not
    public function checkEmailExists(Request $request){

        $query = Admin::query();
        $query->where('email',$request->email);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $admin = $query->first();

        if(!is_null($admin)){
            return 'false';
        } else {
            return 'true';
        }
    }
}
