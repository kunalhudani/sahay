<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\ClientDepartment;
use App\Http\Controllers\Controller;

class ClientDepartmentController extends Controller{
    
    public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //department list
    public function clientDepartmentList(){

        $get_client_department = ClientDepartment::where('is_active',1)->where('is_delete',0)->get();

        return view('admin.client_department.client_department_list',compact('get_client_department'));
    }

    //add department
    public function addClientDepartment(){

        return view('admin.client_department.add_client_department');
    }

    //save client department
    public function saveClientDepartment(Request $request){
    	
        $save_department = new ClientDepartment;
        $save_department->department_name = $request->department_name;
        $save_department->save();

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addClientDepartment'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Department',
                      'message' => 'Department Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.clientDepartmentList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Department',
                      'message' => 'Department Successfully Added',
                  ],
            ]); 
        }
    }

    //edit clint department
    public function editClientDepartment($id){

        $find_client_department = ClientDepartment::where('id',$id)->first();

        return view('admin.client_department.edit_client_department',compact('find_client_department'));
    }

    //upadate client department
    public function saveEditedClientDepartment(Request $request){
    	
        $save_department = ClientDepartment::findOrFail($request->id);
        $save_department->department_name = $request->department_name;
        $save_department->save();

        return redirect(route('admin.clientDepartmentList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Department',
                  'message' => 'Department Successfully Updated',
              ],
        ]); 
    }

    //delete client department
    public function deleteClientDepartment($id){

        $delete_client_department = ClientDepartment::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.clientDepartmentList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Department',
                  'message' => 'Department Successfully Deleted',
              ],
        ]); 

    }
    
    //check department already exists or not
    public function checkDepartmentExists(Request $request){

        $query = ClientDepartment::query();
        $query->where('department_name',$request->department_name);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $department_name = $query->first();

        if(!is_null($department_name)){
            return 'false';
        } else {
            return 'true';
        }
    }
}
