<?php

namespace App\Http\Controllers\Admin;

use App\Model\TaskStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskStatusController extends Controller
{
    public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //task status listing
    public function taskStatusList(){

        $get_task_status = TaskStatus::where('is_delete',0)->get();

        return view('admin.task_status.task_status_list',compact('get_task_status'));
    }

    //add task status form
    public function addTaskStatus(){

        return view('admin.task_status.add_task_status');
    }

    //save task status details
    public function saveTaskStatus(Request $request){
    	
        $save_task = new TaskStatus;
        $save_task->task_status = $request->task_status;
        $save_task->save();

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addTaskStatus'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Task',
                      'message' => 'Task Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.taskStatusList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Task',
                      'message' => 'Task Successfully Added',
                  ],
            ]); 
        }
    }

    //edit task status form
    public function editTaskStatus($id){

        $find_task_status = TaskStatus::where('id',$id)->first();

        return view('admin.task_status.edit_task_status',compact('find_task_status'));
    }

    //upadate task status details
    public function saveEditedTaskStatus(Request $request){
    	
        $save_department = TaskStatus::findOrFail($request->id);
        $save_department->task_status = $request->task_status;
        $save_department->save();

        return redirect(route('admin.taskStatusList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Task',
                  'message' => 'Task Successfully Updated',
              ],
        ]); 
    }

    //remove task status
    public function deleteTaskStatus($id){

        $delete_task_status = TaskStatus::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.taskStatusList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Task',
                  'message' => 'Task Successfully Deleted',
              ],
        ]); 

    }
      
    //check task status already exists or not
    public function checkTaskExists(Request $request){

        $query = TaskStatus::query();
        $query->where('task_status',$request->task_status);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $task_status = $query->first();

        if(!is_null($task_status)){
            return 'false';
        } else {
            return 'true';
        }
    }
}
