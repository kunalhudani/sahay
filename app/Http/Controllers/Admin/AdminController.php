<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use Redirect;
use Validator;
use App\Model\User;
use App\Model\Admin;
use App\Model\State;
use App\Model\Country;
use App\Model\Service;
use App\Model\Company;
use App\Model\Industry;
use App\Model\Consultant;
use App\Model\AdminCompany;
use Illuminate\Http\Request;
use App\Model\ClientDepartment;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\GlobalController;

class AdminController extends GlobalController
{
    public function __construct(){
       $this->middleware('admin');
    }

    //Dashboard 
    public function index(){

        $employee = Admin::where('is_active',1)->where('is_delete',0)->count();

        $department = ClientDepartment::where('is_active',1)->where('is_delete',0)->count();

        $industry = Industry::where('is_delete',0)->count();

        $consultant = Consultant::where('is_active',1)->where('is_delete',0)->count();

        return view('admin.dashboard.dashboard',compact('employee','department','industry','consultant'));
    }
    
    //admin details
    public function adminProfile(){
        
        $admin_profile = Admin::where('id',Auth::guard('admin')->user()->id)->first();
        
        return view('admin.dashboard.admin_update_profile',compact('admin_profile'));
    }

    //update admin details
    public function adminProfileUpdate(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $update_admin = Admin::findOrFail($request->id);
        $update_admin->name = $request->name;
        $update_admin->email = $request->email;
        $update_admin->save(); 

        return redirect()->route('admin.dashboard')->with(['message'=>'Profile successfully updated!','alert-type' => 'success']);
    }

    //change password
    public function changeAdminPassword(){

        return view('admin.dashboard.change_password');
    }

    //update password
    public function updateAdminPassword(Request $request){

        $this->validate($request, [
            'old_pass' => 'required',
            'new_pass' => 'required'
        ]);

        $adminId = Auth::guard('admin')->user()->id;
        $user = Admin::where('id', '=', $adminId)->first();

        if(Hash::check($request->old_pass,$user->password)){

            $users = Admin::findOrFail($adminId);
            $users->password = Hash::make($request->new_pass);
            $users->save();

            return redirect(route('admin.dashboard'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Password',
                      'message' => 'Password Successfully changed',
                  ],
            ]); 

        } else {
          
            return redirect()->back()->with('messages', [
                  [
                      'type' => 'danger',
                      'title' => 'Password',
                      'message' => 'Plese check your current password',
                  ],
            ]); 
        }
    }

    //Help Support Doctor List
    public function helpSupport(){

        $getRequest = HelpSupport::with(['doctor'])->orderBy('status','asc')->get();

        return view('admin.dashboard.help_support_request',compact('getRequest'));
    }

    //attend support request
    public function attendSupportRequest($id){

        $attendSuportRequest = HelpSupport::where('id',$id)->update(['status' => 2]);

        return redirect()->back()->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Password',
                  'message' => 'Support request successfully attened!',
              ],
        ]); 

    }

    //get notifation count
    public function getNotificationCount(){

        $getRequest = HelpSupport::where('status',1)->count();

        return $getRequest;
    }

    public function getMessageDetails(Request $request){

        $getMessage = HelpSupport::where('id',$request->id)->first();

        return $getMessage->message;
    }

    public function changeSocialMediaLink(){
        $updated_link = SocialMediaLink::first();

        return view('admin.dashboard.change_url',compact('updated_link'));   
    }

    public function updateSocialMediaLink(Request $request){
        $link = SocialMediaLink::where('user_id',$request->id)->delete();
            $users = new SocialMediaLink;
            $users->user_id = $request->id;
            $users->link = $request->link;
            $users->save();
        
        if($users){

            return redirect(route('admin.dashboard'))->with('messages', [
                [
                  'type' => 'success',
                  'title' => 'Link',
                  'message' => 'Link Successfully changed',
                ],
            ]);    
        }
  
    }
   	
   	public function getAutoSuggestState(Request $request){

        $getStateSuggestion = State::get();
        
        $stateJson = array();
        if(!is_null($getStateSuggestion)){
            foreach($getStateSuggestion as $sk => $sv){

                $stateJson[$sk]['label'] = $sv->state;
                $stateJson[$sk]['value'] = $sv->id;
                $stateJson[$sk]['new_value'] = $sv->state_code;

            }
        }

        return $stateJson; 
    }

    public function getAutoSuggestCountry(Request $request){

        $getAutoSuggestCountry = Country::get();

        $countryJson = array();
        if(!is_null($getAutoSuggestCountry)){
            foreach($getAutoSuggestCountry as $sk => $sv){
                $countryJson[$sk]['label'] = $sv->country_name;
                $countryJson[$sk]['value'] = $sv->id;
            }
        }

        return $countryJson; 
    }

    public function getAutoSuggestClient(Request $request){

        $getClientSuggestion = Company::with(['state'])->where('is_active',1)->where('is_delete',0)->get();
        
        $clientJson = array();

        if(!is_null($getClientSuggestion)){
            foreach($getClientSuggestion as $ck => $cv){
                $clientJson[$ck]['label'] = $cv->company_name;
                $clientJson[$ck]['client_id'] = $cv->id;
                $clientJson[$ck]['address'] = $cv->address;
                $clientJson[$ck]['gstn'] = $cv->gstn;
                $clientJson[$ck]['state_id'] = $cv->state_id;
                if(!is_null($cv->state)){

                    $clientJson[$ck]['state'] = $cv->state->state;
                }else{
                    $clientJson[$ck]['state'] = '';
                }
                if($cv->state_code != ''){
                    $clientJson[$ck]['state_code'] = $cv->state_code;
                }else{
                   $clientJson[$ck]['state_code'] = ''; 
                }
                $consultants = array();
                if(!is_null($cv['linked_consultants'])){
                    foreach ($cv['linked_consultants'] as $lk => $lv) {
                        $consultants[] = $lv['consultant_id'];
                    }
                }
                if(!is_null($consultants)){
                    $clientJson[$ck]['consultants'] = $consultants; 
                } 
            }
        }

        return $clientJson;    
    }

    public function companyProfile(Request $request){

        if(!empty($request->all())){

            if(isset($request->id) && ($request->id != '')){

                $company = AdminCompany::where('id',$request->id)->first();
                $delete = AdminCompany::where('id',$company->id)->delete();

            }

            $company_profile_detail = New AdminCompany;

            $uuid = $this->generateUUID();
            $company_profile_detail->uuid = $uuid;
            if($request->profile_image != ''){
                $this->uploadProfile($uuid,$request->profile_image);
                $company_profile_detail->logo = $request->profile_image;
            }
            $company_profile_detail->company_name = $request->company_name;
            $company_profile_detail->company_address = $request->company_address;
            /*$company_profile_detail->state = $request->company_state;*/
            //state is not in database than add
            $check_state = State::where('state','LIKE','%'.$request->company_state.'%')->first();
            if($check_state == '' && empty($check_state)){

                $add_state = new State;
                $add_state->state = $request->company_state;
                $add_state->save();

                $company_profile_detail->state = $add_state->id;
            }else{

                $company_profile_detail->state = $request->state_id;
            }
            $company_profile_detail->state_code = $request->state_code;
            $company_profile_detail->country = $request->country;
            $company_profile_detail->pancard_number = strtoupper($request->pancard_number);
            $company_profile_detail->gstn_number = strtoupper($request->gstn_number);
            $company_profile_detail->bank_name = $request->bank_name;
            $company_profile_detail->bank_branch = $request->bank_branch;
            $company_profile_detail->account_number = $request->account_number;
            $company_profile_detail->ifsc_code = strtoupper($request->ifsc_code);
            $company_profile_detail->micr_number = $request->micr_number;
            $company_profile_detail->save();

            $company_profile_detail = AdminCompany::with(['state_name','country_detail'])->first();

        }else{

            $company_profile_detail = AdminCompany::with(['state_name','country_detail'])->first();
            if(empty($company_profile_detail)){
                $company_profile_detail = '';
            }

        }
        
        return view('admin.dashboard.company_profile',compact('company_profile_detail'));
        
    }
}