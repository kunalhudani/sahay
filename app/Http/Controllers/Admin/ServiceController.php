<?php

namespace App\Http\Controllers\Admin;

use App\Model\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

	//service status listing
    public function serviceList(){

        $get_services = Service::where('is_delete',0)->get();

        return view('admin.service.service_list',compact('get_services'));
    }

    //add service status form
    public function addService(){

        return view('admin.service.add_service');
    }

    //save service status details
    public function saveService(Request $request){
    	
        $save_service = new Service;
        $save_service->service_name = $request->service_name;
        if($request->hsn != ''){
        	$save_service->hsn = $request->hsn;
        }
        $save_service->sgst = $request->sgst;
        $save_service->cgst = $request->cgst;
        $save_service->gst = $request->gst;
        $save_service->icgst = $request->icgst;
        $save_service->taxation_type = $request->taxation_type;
        if($request->taxation_type == 2){
        	$save_service->tax = $request->tax;
        }	
        $save_service->save();

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.serviceList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Service',
                      'message' => 'Service Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.serviceList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Service',
                      'message' => 'Service Successfully Added',
                  ],
            ]); 
        }
    }

    //edit service status form
    public function editService($id){

        $find_service = Service::where('id',$id)->first();

        return view('admin.service.edit_service',compact('find_service'));
    }

    //upadate service status details
    public function saveEditedService(Request $request){
    	
        $save_service = Service::findOrFail($request->id);
        $save_service->service_name = $request->service_name;
        if($request->hsn != ''){
        	$save_service->hsn = $request->hsn;
        }
        $save_service->sgst = $request->sgst;
        $save_service->cgst = $request->cgst;
        $save_service->gst = $request->gst;
        $save_service->icgst = $request->icgst;
        $save_service->taxation_type = $request->taxation_type;
        if($request->taxation_type == 2){
        	$save_service->tax = $request->tax;
        }	
        $save_service->save();

        return redirect(route('admin.serviceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Service',
                  'message' => 'Service Successfully Updated',
              ],
        ]); 
    }

    //remove service status
    public function deleteService($id){

        $delete_service = Service::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.serviceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Service',
                  'message' => 'Service Successfully Deleted',
              ],
        ]); 

    }
      
    //check service status already exists or not
    public function checkService(Request $request){
        
        $query = Service::query();
        $query->where('service_name',$request->service_name);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $service = $query->first();

        if(!is_null($service)){
            return 'false';
        } else {
            return 'true';
        }

    }

    public function checkHSN(Request $request){

      $query = Service::query();
      $query->where('hsn',$request->hsn);
      if (isset($request->id)) {
          $query->where('id','!=',$request->id);
      }
      $service = $query->first();
      
      if(!is_null($service)){
          return 'false';
      } else {
          return 'true';
      }

    }
}
