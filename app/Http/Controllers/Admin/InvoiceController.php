<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Model\Company;
use App\Model\Service;
use App\Model\Invoice;
use App\Model\Consultant;
use App\Model\ReminderLog;
use App\Model\AdminCompany;
use App\Model\PaymentDetail;
use Illuminate\Http\Request;
use App\Model\InvoiceService;
use App\Http\Controllers\Controller;
use App\Model\GenerateInvoiceDetail;
use App\Model\GenerateInvoiceService;
use App\Model\InvoiceProjectIncharge;
use App\Model\GenerateInvoiceProjectIncharge;
use App\Http\Controllers\GlobalController;

class InvoiceController extends GlobalController
{
    public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //invoice type listing
    public function invoiceList(){

        $get_invoice = Invoice::with(['company','consultant','service','payment_detail'])->where('is_delete',0)->orderBy('id', 'DESC')->get();
        
        if(!is_null($get_invoice)){
            $payment = [];
            foreach ($get_invoice as $gk => $gv) {
                
                $get_all_payment = PaymentDetail::with(['payment_amount'])->where('invoice_id',$gv->id)->orderBy('id', 'DESC')->where('is_delete',0)->get();

                $get_all_payment_amount = [];
                foreach ($get_all_payment as $pk => $pv) {
                    if($gv->id == $pv->invoice_id){
                        if($pv->tds_amount != ''){

                            $get_all_payment_amount[] = $pv->paid_amount + $pv->tds_amount;
                        }else{
                            $get_all_payment_amount[] = $pv->paid_amount;
                        }
                    }

                    $payment[$gv->id] = $get_all_payment_amount;
                }
                
            }
        }

        return view('admin.invoice.invoice_list',compact('get_invoice','payment'));
    }

    public function addInvoice(){

    	$get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();

    	return view('admin.invoice.add_invoice',compact('get_consultant'));
    }

    //fetch last performa company details
    public function getCompanyDetails(Request $request){
        
        $find_invoice = Invoice::with(['company','state','consultant','service'])->where('client_id',$request->client_id)->first();

        $find_incharge = InvoiceProjectIncharge::where('invoice_id',$find_invoice->id)->pluck('project_incharge')->toArray();

        $get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();

        return view('admin.invoice.get_company_details',compact('get_consultant','find_incharge','find_invoice'));
    }

    public function saveInvoice(Request $request){
        
    	$save_invoice = New Invoice;
    	$save_invoice->client_id = $request->client_id;
    	$save_invoice->address = $request->address;
    	$save_invoice->gstn = $request->gstn;
    	$save_invoice->invoice_no = $request->invoice_no;
    	if($request->invoice_date != ''){
    		$save_invoice->invoice_date = $this->convertDate($request->invoice_date);
    	}else{
    		$save_invoice->invoice_date = '';
    	}
    	if($request->due_date != ''){
    		$save_invoice->due_date = $this->convertDate($request->due_date);
    	}else{
    		$save_invoice->due_date = '';
    	}
    	$save_invoice->state_of_supply = $request->state_id;
    	$save_invoice->state_code = $request->state_code;
    	$save_invoice->total_sub_total = $request->total_sub_total;
    	$save_invoice->total_payable = $request->total_payable;
        $save_invoice->tax_type = $request->tax_type;
        //2 for differnt state tax 3 for international 1 for same state tax
        if($request->tax_type == 2 || $request->tax_type == 3){
            $save_invoice->total_tax_amount = $request->total_tax_amount;
        }else{
            $save_invoice->total_sgst = $request->total_sgst;
            $save_invoice->total_cgst = $request->total_cgst;
        }
    	$save_invoice->save();
    	
    	if(!is_null($request->consultant)){
    		foreach ($request->consultant as $ck => $cv) {
    			
    			$save_consultant = New InvoiceProjectIncharge;
    			$save_consultant->invoice_id = $save_invoice->id;
    			$save_consultant->project_incharge = $cv;
    			$save_consultant->save();
    		}
    	}

    	if(!is_null($request->service)){
    		foreach ($request->service as $sk => $sv) {
    			
    			$save_service = New InvoiceService;
    			$save_service->invoice_id = $save_invoice->id;
    			$save_service->name = $sv['name'];
    			$save_service->service_id = $sv['service_id'];
    			$save_service->hsn = $sv['hsn'];
    			$save_service->description = $sv['description'];
    			$save_service->quantity = $sv['qty'];
    			$save_service->rate = $sv['rate'];
    			$save_service->subtotal = $sv['subtotal'];
    			$save_service->tax = $sv['tax'];
                $save_service->total_tax_payment = $sv['total_tax_payment'];
                $save_service->total_sgst_payment = $sv['total_sgst_payment'];
    			$save_service->save();
    		}
    	}

    	return redirect(route('admin.invoiceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Invoice',
                  'message' => 'Invoice Successfully Added',
              ],
        ]); 
    }

    public function editInvoice($id){

    	$find_invoice = Invoice::with(['company','state','consultant','service'])->where('id',$id)->first();

        $find_incharge = InvoiceProjectIncharge::where('invoice_id',$id)->pluck('project_incharge')->toArray();

    	$get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();
    	

    	return view('admin.invoice.edit_invoice',compact('find_invoice','get_consultant','find_incharge'));
    }

    public function saveEditedInvoice(Request $request){

    	$save_invoice = Invoice::findOrFail($request->invoice_id);
    	$save_invoice->client_id = $request->client_id;
    	$save_invoice->address = $request->address;
    	$save_invoice->gstn = $request->gstn;
    	$save_invoice->invoice_no = $request->invoice_no;
    	if($request->invoice_date != ''){
    		$save_invoice->invoice_date = $this->convertDate($request->invoice_date);
    	}else{
    		$save_invoice->invoice_date = '';
    	}
    	if($request->due_date != ''){
    		$save_invoice->due_date = $this->convertDate($request->due_date);
    	}else{
    		$save_invoice->due_date = '';
    	}
    	$save_invoice->state_of_supply = $request->state_id;
    	$save_invoice->state_code = $request->state_code;
    	$save_invoice->total_sub_total = $request->total_sub_total;
    	$save_invoice->total_tax_amount = $request->total_tax_amount;
    	$save_invoice->total_payable = $request->total_payable;
    	$save_invoice->save();

    	$deleteProjectIncharge = InvoiceProjectIncharge::where('invoice_id',$request->invoice_id)->delete();
    	if(!is_null($request->consultant)){
    		foreach ($request->consultant as $ck => $cv) {
    			
    			$save_consultant = New InvoiceProjectIncharge;
    			$save_consultant->invoice_id = $request->invoice_id;
    			$save_consultant->project_incharge = $cv;
    			$save_consultant->save();
    		}
    	}

    	$deleteService = InvoiceService::where('invoice_id',$request->invoice_id)->delete();
    	if(!is_null($request->service)){
    		foreach ($request->service as $sk => $sv) {
    			
    			$save_service = New InvoiceService;
    			$save_service->invoice_id = $request->invoice_id;
    			$save_service->name = $sv['name'];
    			$save_service->service_id = $sv['service_id'];
    			$save_service->hsn = $sv['hsn'];
    			$save_service->description = $sv['description'];
    			$save_service->quantity = $sv['qty'];
    			$save_service->rate = $sv['rate'];
    			$save_service->subtotal = $sv['subtotal'];
    			$save_service->tax = $sv['tax'];
    			$save_service->save();
    		}
    	}

    	return redirect(route('admin.invoiceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Invoice',
                  'message' => 'Invoice Successfully Updated',
              ],
        ]);
    }

    public function deleteInvoice($id){

        $delete_service = Invoice::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.invoiceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Invoice',
                  'message' => 'Invoice Successfully Deleted',
              ],
        ]);
    }

    //send reminder to client
    public function sendClientReminder(Request $request){

        $get_invoice = Invoice::with(['company' => function($q){ $q->with(['employee' => function($q){ $q->with(['employee']);}]); }])->whereIn('id',$request->checkbox)->get();
        

        if(!is_null($get_invoice)){
            foreach ($get_invoice as $gk => $gv) {
                if(!is_null($gv['company']['employee'])){
                    foreach ($gv['company']['employee'] as $ek => $ev) {
                       
                       
                        if($ev['type'] == 1){
                            $templete = 'invoice_mail';
                            $email = $ev['employee']['email'];
                            $sub = 'Invoice';
                            $mail_message = 'Invoice Reminder';
                            $this->sendMail($templete,$mail_message,$sub,$email);

                            $save_reminder = New ReminderLog;
                            $save_reminder->invoice_id = $gv->id;
                            $save_reminder->company_id = $gv->client_id;
                            $save_reminder->employee_id = $ev['employee']['id'];
                            $save_reminder->save();
                        }
                    }
                }
            }
        }

        return redirect(route('admin.invoiceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Reminder',
                  'message' => 'Reminder Successfully Send',
              ],
        ]);  
    }

    public function saveExpected(Request $request){

        $expected_date = $this->convertDate($request->expected_date);
        $delete_service = Invoice::where('id',$request->id)->update(['expected_payment_date' => $expected_date]);

        if($delete_service){

            return 'true';

        }else{

            return 'false';
        }
    }

    public function sendPerforma($id){

        $get_invoice = Invoice::with(['company' => function($q){ $q->with(['employee' => function($q){ $q->with(['employee']);}]); }])->where('id',$id)->first();

        if(!is_null($get_invoice['company']['employee'])){
            foreach ($get_invoice['company']['employee'] as $ek => $ev) {
               
               
                if($ev['type'] == 1){
                    $templete = 'invoice_mail';
                    $email = $ev['employee']['email'];
                    $sub = 'Invoice';
                    $mail_message = 'Invoice Reminder';
                    $this->sendMail($templete,$mail_message,$sub,$email);

                    $save_reminder = New ReminderLog;
                    $save_reminder->invoice_id = $get_invoice->id;
                    $save_reminder->company_id = $get_invoice->client_id;
                    $save_reminder->employee_id = $ev['employee']['id'];
                    $save_reminder->save();
                }
            }
        }

        return redirect(route('admin.invoiceList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Performa',
                  'message' => 'Performa Successfully Send',
              ],
        ]);  
    }
    //end of invoices 

    //payment controllers
    public function paymentDetail($id){

        $find_invoice = Invoice::with(['company','state','consultant','service'])->where('id',$id)->first();

        $get_all_payment = PaymentDetail::with(['payment_amount'])->where('invoice_id',$id)->orderBy('id', 'DESC')->where('is_delete',0)->get();

        $get_all_payment_amount = [];
        foreach ($get_all_payment as $gk => $gv) {
            
            if($gv->tds_amount != ''){

                $get_all_payment_amount[$gk] = $gv->paid_amount + $gv->tds_amount;
            }else{
                $get_all_payment_amount[$gk] = $gv->paid_amount;
            }
            
        }
        $sum_payment = array_sum($get_all_payment_amount);
        
        return view('admin.invoice.payment.payment_detail',compact('id','find_invoice','get_all_payment','sum_payment'));
            
    }

    public function savePaymentDetail(Request $request){

        $payment_detail = New PaymentDetail;
        $payment_detail->invoice_id = $request->invoice_id;
        $payment_detail->date_of_entery = $this->convertDate($request->date_of_entery);
        $payment_detail->paid_amount = $request->paid_amount;
        $payment_detail->tax_amount = $request->tax_amount;
        $payment_detail->tds_deducted = $request->tds_deducted;
        $payment_detail->tds_amount = $request->tds_amount;
        $payment_detail->save();

        if($request->btn_submit == 'save'){

            return redirect(route('admin.paymentDetail',$request->invoice_id))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Payment',
                      'message' => 'Payment Detail Successfully Added',
                  ],
            ]); 

        } 
    }

    public function saveEditedPaymentDetail(Request $request){

        $payment_detail = PaymentDetail::findOrFail($request->id);
        $payment_detail->invoice_id = $request->invoice_id;
        $payment_detail->date_of_entery = $this->convertDate($request->date_of_entery);
        $payment_detail->paid_amount = $request->paid_amount;
        $payment_detail->tax_amount = $request->tax_amount;
        $payment_detail->tds_deducted = $request->tds_deducted;
        $payment_detail->tds_amount = $request->tds_amount;
        $payment_detail->save();

        if($request->btn_submit == 'save'){

            return redirect(route('admin.paymentDetail',$request->invoice_id))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Payment',
                      'message' => 'Payment Detail Successfully Updated',
                  ],
            ]); 

        } 
    }

    public function generateInvoice($id = null,$invoice_id = null){
        
        $payment_detail = PaymentDetail::where('id',$id)->first();

        $find_invoice = Invoice::with(['company','state','consultant','service'])->where('id',$invoice_id)->first();

        $find_incharge = InvoiceProjectIncharge::where('invoice_id',$invoice_id)->pluck('project_incharge')->toArray();

        $get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();

        return view('admin.invoice.payment.generate_invoice',compact('invoice_id','find_invoice','payment_detail','get_consultant','find_incharge'));
    }

    public function saveEditedGenerateInvoice(Request $request){
        
        $save_invoice = New GenerateInvoiceDetail;
        $save_invoice->payment_id = $request->id;
        $save_invoice->invoice_id = $request->invoice_id;
        $save_invoice->client_id = $request->client_id;
        $save_invoice->address = $request->address;
        $save_invoice->gstn = $request->gstn;
        $save_invoice->paid_amount = $request->paid_amount;
        $save_invoice->tax_amount = $request->tax_amount;
        $save_invoice->tds_amount = $request->tds_amount;
        $save_invoice->invoice_no = $request->invoice_no;
        if($request->invoice_date != ''){
            $save_invoice->invoice_date = $this->convertDate($request->invoice_date);
        }else{
            $save_invoice->invoice_date = '';
        }
        if($request->due_date != ''){
            $save_invoice->due_date = $this->convertDate($request->due_date);
        }else{
            $save_invoice->due_date = '';
        }
        $save_invoice->state_of_supply = $request->state_id;
        $save_invoice->state_code = $request->state_code;
        $save_invoice->total_sub_total = $request->total_sub_total;
        $save_invoice->total_payable = $request->total_payable;
        $save_invoice->tax_type = $request->tax_type;
        //2 for differnt state tax 3 for international 1 for same state tax
        if($request->tax_type == 2 || $request->tax_type == 3){
            $save_invoice->total_tax_amount = $request->total_tax_amount;
        }else{
            $save_invoice->total_sgst = $request->total_sgst;
            $save_invoice->total_cgst = $request->total_cgst;
        }
        $save_invoice->save();
        
        if(!is_null($request->consultant)){
            foreach ($request->consultant as $ck => $cv) {
                
                $save_consultant = New GenerateInvoiceProjectIncharge;
                $save_consultant->invoice_id = $save_invoice->id;
                $save_consultant->project_incharge = $cv;
                $save_consultant->save();
            }
        }

        if(!is_null($request->service)){
            foreach ($request->service as $sk => $sv) {
                
                $save_service = New GenerateInvoiceService;
                $save_service->invoice_id = $save_invoice->id;
                $save_service->name = $sv['name'];
                $save_service->service_id = $sv['service_id'];
                $save_service->hsn = $sv['hsn'];
                $save_service->description = $sv['description'];
                $save_service->quantity = $sv['qty'];
                $save_service->rate = $sv['rate'];
                $save_service->subtotal = $sv['subtotal'];
                $save_service->tax = $sv['tax'];
                $save_service->total_tax_payment = $sv['total_tax_payment'];
                $save_service->total_sgst_payment = $sv['total_sgst_payment'];
                $save_service->save();
            }
        }

        $payment_data = PaymentDetail::where('id',$request->id)->update(['is_invoice_generated' => 1]);

        return redirect(route('admin.paymentDetail',$save_invoice->invoice_id))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Invoice Genrate',
                  'message' => 'Invoice Genrated Successfully',
              ],
        ]);
    }

    public function deletePaymentDetail($id = null,$invoice_id = null){

        //delete genrated invoices
        $deleted_genrated_invoices = GenerateInvoiceDetail::where('payment_id',$id)->update(['is_delete' =>  1]);

        //delete payment details
        $delete_service = PaymentDetail::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.paymentDetail',$invoice_id))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Payment Detail',
                  'message' => 'Payment Detail Successfully Deleted',
              ],
        ]);
    }

    //end of payment controller

    //genrated invoice controller
    public function genratedInvoiceList($id){

        $get_genrated_invoices = GenerateInvoiceDetail::with(['company','service','consultant' => function($q){ $q->with(['project_incharge_name']); }])->where('invoice_id',$id)->where('is_delete',0)->orderBy('id', 'DESC')->get();
        
        return view('admin.invoice.generate_invoice.generate_invoice_list',compact('get_genrated_invoices','id'));
    }

    public function editGenratedInvoice($id = null,$invoice_id = null){

        $find_invoice = GenerateInvoiceDetail::with(['company','state','consultant','service'])->where('id',$id)->where('invoice_id',$invoice_id)->first();

        $find_incharge = GenerateInvoiceProjectIncharge::where('invoice_id',$id)->pluck('project_incharge')->toArray();

        $get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();
        
        return view('admin.invoice.generate_invoice.edit_genrated_invoice',compact('find_invoice','get_consultant','find_incharge'));  
    }

    public function updatedGeneratedInvoice(Request $request){
       
        $save_invoice = GenerateInvoiceDetail::findOrFail($request->id);
        $save_invoice->client_id = $request->client_id;
        $save_invoice->address = $request->address;
        $save_invoice->gstn = $request->gstn;
        $save_invoice->paid_amount = $request->paid_amount;
        $save_invoice->tax_amount = $request->tax_amount;
        $save_invoice->tds_amount = $request->tds_amount;
        $save_invoice->invoice_no = $request->invoice_no;
        if($request->invoice_date != ''){
            $save_invoice->invoice_date = $this->convertDate($request->invoice_date);
        }else{
            $save_invoice->invoice_date = '';
        }
        if($request->due_date != ''){
            $save_invoice->due_date = $this->convertDate($request->due_date);
        }else{
            $save_invoice->due_date = '';
        }
        $save_invoice->state_of_supply = $request->state_id;
        $save_invoice->state_code = $request->state_code;
        $save_invoice->total_sub_total = $request->total_sub_total;
        $save_invoice->total_tax_amount = $request->total_tax_amount;
        $save_invoice->total_payable = $request->total_payable;
        $save_invoice->save();

        $deleteProjectIncharge = GenerateInvoiceProjectIncharge::where('invoice_id',$request->id)->delete();
        if(!is_null($request->consultant)){
            foreach ($request->consultant as $ck => $cv) {
                
                $save_consultant = New GenerateInvoiceProjectIncharge;
                $save_consultant->invoice_id = $request->id;
                $save_consultant->project_incharge = $cv;
                $save_consultant->save();
            }
        }

        $deleteService = GenerateInvoiceService::where('invoice_id',$request->id)->delete();
        if(!is_null($request->service)){
            foreach ($request->service as $sk => $sv) {
                
                $save_service = New GenerateInvoiceService;
                $save_service->invoice_id = $request->invoice_id;
                $save_service->name = $sv['name'];
                $save_service->service_id = $sv['service_id'];
                $save_service->hsn = $sv['hsn'];
                $save_service->description = $sv['description'];
                $save_service->quantity = $sv['qty'];
                $save_service->rate = $sv['rate'];
                $save_service->subtotal = $sv['subtotal'];
                $save_service->tax = $sv['tax'];
                $save_service->save();
            }
        }

        return redirect(route('admin.genratedInvoiceList',$request->invoice_id))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Genrated Invoice',
                  'message' => 'Genrated Invoice Successfully Updated',
              ],
        ]);
    }

    public function deleteGeneratedInvoice($id,$invoice_id = null){

        $get_payment_id = GenerateInvoiceDetail::where('id',$id)->first();

        $payment_data = PaymentDetail::where('id',$get_payment_id->payment_id)->update(['is_invoice_generated' => 0]);

        $delete_generated = GenerateInvoiceDetail::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.genratedInvoiceList',$invoice_id))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Genrated Invoice',
                  'message' => 'Genrated Invoice Successfully Deleted',
              ],
        ]);
    }

    public function sendGeneratedInvoice($id,$invoice_id = null){

        /*echo $id;
        echo "<br>";
        echo $invoice_id;
        exit;

        $data = ['title' => 'Invoice'];*/

        $find_invoice = GenerateInvoiceDetail::with(['company','state','consultant','service'])->where('id',$id)->where('invoice_id',$invoice_id)->first();

        $find_incharge = GenerateInvoiceProjectIncharge::where('invoice_id',$id)->pluck('project_incharge')->toArray();

        $get_consultant = Consultant::where('is_active',1)->where('is_delete',0)->get();


        $pdf = PDF::loadView('admin.invoice.generate_invoice.pdf_genrated_invoice',compact('find_invoice','find_incharge','get_consultant'));
        
        return $pdf->download('invoice.pdf');

        /*return view('admin.invoice.generate_invoice.pdf_genrated_invoice',compact('find_invoice','get_consultant','find_incharge'));  */

    }

    //end of genrated invoice controller

    //reminder listing
    public function reminderList($id){

        $get_reminder = ReminderLog::with(['company','employee'])->where('invoice_id',$id)->get();

        return view('admin.invoice.reminder_log.reminder_log_list',compact('get_reminder'));
    }

    //service name with other data
    public function getAutoSuggestService(Request $request){

        $get_company = AdminCompany::first();
        $get_client_country = Company::where('id',$request->client_id)->first();

        $getAutoSuggestService = Service::where('is_delete',0)->get();

        $serviceJson = array();
        if(!is_null($getAutoSuggestService)){
            foreach($getAutoSuggestService as $gk => $gv){
                $serviceJson[$gk]['label'] = $gv->service_name;
                $serviceJson[$gk]['value'] = $gv->id;
                $serviceJson[$gk]['hsn'] = $gv->hsn;

                //international
                if($get_client_country->country_id != $get_company->country){

                    if($gv->tax == ''){
                        $serviceJson[$gk]['tax'] = 0;
                    }else{
                        $serviceJson[$gk]['tax'] = $gv->tax;    
                    } 
                    $serviceJson[$gk]['tax_type'] = 3;

                }else{
                    //same state
                    if($request->state_code == $get_company->state_code){

                        if($gv->gst == ''){
                            $serviceJson[$gk]['tax'] = 0;
                        }else{
                            $serviceJson[$gk]['tax'] = $gv->gst;    
                        }
                        $serviceJson[$gk]['sgst'] = $gv->sgst;
                        $serviceJson[$gk]['cgst'] = $gv->cgst;
                        $serviceJson[$gk]['tax_type'] = 1;
                    //different state
                    }elseif ($request->state_code != $get_company->state_code) {
                        
                        if($gv->icgst == ''){
                            $serviceJson[$gk]['tax'] = 0;
                        }else{
                            $serviceJson[$gk]['tax'] = $gv->icgst;    
                        } 
                        $serviceJson[$gk]['tax_type'] = 2;

                    }
                }
                
            }
        }

        return $serviceJson; 

    }
    
}
