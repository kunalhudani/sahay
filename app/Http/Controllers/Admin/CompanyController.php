<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
use App\Model\City;
use App\Model\Service;
use App\Model\ClientDepartment;
use App\Model\Company;
use App\Model\Admin;
use App\Model\State;
use App\Model\CompanyEmployee;
use App\Model\LinkedCompanyConsultant;
use App\Model\LinkedCompanyEmployee;
use App\Model\CompanyEngagement;
use App\Model\CompanyLocation;
use App\Model\CompanyModule;
use App\Model\Employee;
use App\Model\Industry;
use App\Model\Consultant;
use App\Model\EmployeeModule;
use App\Model\EngagementType;
use Illuminate\Http\Request;

class CompanyController extends GlobalController
{
    public function __construct(){
       	$this->middleware('admin');
       	$this->middleware('checkpermission');
    }

    //company list
    public function companyList(){

    	$company = Company::where('is_delete',0)->with(['employee'])->get();

    	return view('admin.company.company_list',compact('company'));
    }

    //add company
    public function addCompany(){

    	$get_city = City::get();

    	$get_services = Service::where('is_delete',0)->get();
        $get_employees = Admin::where('is_delete',0)->get();
        $get_consultant = Consultant::where('is_delete',0)->get();
        $get_industry = Industry::where('is_delete',0)->get();

    	return view('admin.company.add_company',compact('get_city','get_services','get_employees','get_consultant','get_industry'));
    }

    //save company
    public function saveCompany(Request $request){

    	$uuid = $this->generateUUID();

    	$saveEmployee = new Company;
    	if(isset($request->company_logo)){
            $company_logo = $this->uploadBucket($request->company_logo,'company_logo',$uuid);
            $saveEmployee->company_logo = $company_logo;
        }
        $saveEmployee->uuid = $uuid;
        $saveEmployee->company_name = $request->company_name;
        $saveEmployee->address = $request->address;
        $saveEmployee->contact_no = $request->mobile_number;
        $saveEmployee->gstn = strtoupper($request->gstn_number);
        $saveEmployee->days = $request->days;
        $saveEmployee->industry_type = $request->industry_type;
        /*$saveEmployee->state_id = $request->state_id;*/
        //state is not in database than add
        $check_state = State::where('state','LIKE','%'.$request->company_state.'%')->first();
        if($check_state == '' && empty($check_state)){

            $add_state = new State;
            $add_state->state = $request->company_state;
            $add_state->save();

            $saveEmployee->state_id = $add_state->id;
        }else{

            $saveEmployee->state_id = $request->state_id;
        }
        $saveEmployee->state_code = $request->state_code;
        $saveEmployee->country_id = $request->country_id;
        $saveEmployee->save();

        if(!is_null($request->location)){
            foreach($request->location as $mk => $mv){
                $location = new CompanyLocation;
                $location->company_id = $saveEmployee->id;
                $location->location_id = $mv;
                $location->save();
            }
        }

        if(!is_null($request->engagement)){
            foreach($request->engagement as $ek => $ev){
                $engagement = new CompanyEngagement;
                $engagement->company_id = $saveEmployee->id;
                $engagement->engagements = $ev;
                $engagement->save();
            }
        }

        if(!is_null($request->employees)){
            foreach($request->employees as $ek => $ev){
                $employees = new LinkedCompanyEmployee;
                $employees->company_id = $saveEmployee->id;
                $employees->employee_id = $ev;
                $employees->save();
            }
        }

        if(!is_null($request->consultant)){
            foreach($request->consultant as $ck => $cv){
                $consultant = new LinkedCompanyConsultant;
                $consultant->company_id = $saveEmployee->id;
                $consultant->consultant_id = $cv;
                $consultant->save();
            }
        }

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.companyList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Company',
                      'message' => 'Company Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.companyList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Company',
                      'message' => 'Company Successfully Added',
                  ],
            ]); 
        }
    }

    //edit company
    public function editCompany($id){

    	$location = array();

    	$engagement = array();
        $employees = array();
        $consultant = array();
        
    	$get_city = City::get();

    	$get_services = Service::where('is_delete',0)->get();

    	$getCompany = Company::where('id',$id)->with(['location','engagement','state','country','linked_employees','linked_consultants'])->first();

    	if(!is_null($getCompany->location)){
    		foreach($getCompany->location as $lk => $lv){
    			$location[] = $lv->location_id;
    		}
    	}

		if(!is_null($getCompany->engagement)){
    		foreach($getCompany->engagement as $ek => $ev){
    			$engagement[] = $ev->engagements;
    		}
    	}    	

        if(!is_null($getCompany->linked_employees)){
            foreach($getCompany->linked_employees as $lk => $lv){
                $employees[] = $lv->employee_id;
            }
        }  

        if(!is_null($getCompany->linked_consultants)){
            foreach($getCompany->linked_consultants as $lck => $lcv){
                $consultant[] = $lcv->consultant_id;
            }
        }  

        $get_employees = Admin::where('is_delete',0)->get();
        $get_consultant = Consultant::where('is_delete',0)->get();
        $get_industry = Industry::where('is_delete',0)->get();

    	return view('admin.company.edit_company_list',compact('getCompany','get_city','get_services','location','engagement','employees','consultant','get_employees','get_consultant','get_industry'));
    }

    //save edited company
    public function saveEditedCompany(Request $request){
        
    	$saveEmployee = Company::findOrFail($request->id);
    	if(isset($request->company_logo)){
            $company_logo = $this->uploadBucket($request->company_logo,'company_logo',$uuid);
            $saveEmployee->company_logo = $company_logo;
        }
        $saveEmployee->company_name = $request->company_name;
        $saveEmployee->address = $request->address;
        $saveEmployee->contact_no = $request->mobile_number;
        $saveEmployee->gstn = strtoupper($request->gstn_number);
        $saveEmployee->days = $request->days;
        $saveEmployee->industry_type = $request->industry_type;
        /*$saveEmployee->state_id = $request->state_id;*/
        //state is not in database than add
        $check_state = State::where('state','LIKE','%'.$request->company_state.'%')->first();
        if($check_state == '' && empty($check_state)){

            $add_state = new State;
            $add_state->state = $request->company_state;
            $add_state->save();

            $saveEmployee->state_id = $add_state->id;
        }else{

            $saveEmployee->state_id = $request->state_id;
        }
        $saveEmployee->state_code = $request->state_code;
        $saveEmployee->country_id = $request->country_id;
        $saveEmployee->save();

        $deleteLocation = CompanyLocation::where('company_id',$request->id)->delete();
        if(!is_null($request->location)){
            foreach($request->location as $mk => $mv){
                $location = new CompanyLocation;
                $location->company_id = $saveEmployee->id;
                $location->location_id = $mv;
                $location->save();
            }
        }

        $deleteEngagement = CompanyEngagement::where('company_id',$request->id)->delete();
        if(!is_null($request->engagement)){
            foreach($request->engagement as $ek => $ev){
                $engagement = new CompanyEngagement;
                $engagement->company_id = $saveEmployee->id;
                $engagement->engagements = $ev;
                $engagement->save();
            }
        }

        $delete_employess = LinkedCompanyEmployee::where('company_id',$request->id)->delete();
        if(!is_null($request->employees)){
            foreach($request->employees as $ek => $ev){
                $employees = new LinkedCompanyEmployee;
                $employees->company_id = $saveEmployee->id;
                $employees->employee_id = $ev;
                $employees->save();
            }
        }

        $deleteEngagement = LinkedCompanyConsultant::where('company_id',$request->id)->delete();
        if(!is_null($request->consultant)){
            foreach($request->consultant as $ck => $cv){
                $consultant = new LinkedCompanyConsultant;
                $consultant->company_id = $saveEmployee->id;
                $consultant->consultant_id = $cv;
                $consultant->save();
            }
        }
        
        return redirect(route('admin.companyList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Company',
                  'message' => 'Company Successfully Added',
              ],
        ]); 
    }

    //delete company
    public function deleteCompany($id){

    	$deleteEmployee = Company::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.companyList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Company',
                  'message' => 'Company Successfully Deleted',
              ],
        ]); 
    }

    public function companyTeamList($uuid){

        $getCompanyId = $this->getCompanyId($uuid);

        $getCompanyUser = CompanyEmployee::where('company_id',$getCompanyId)->with(['employee'])->get();

        return view('admin.company.company_user',compact('getCompanyUser','uuid'));
    }

    public function addCompanyUser($uuid){

        $getCompanyId = $this->getCompanyId($uuid);

        $module = CompanyModule::all();

        $clientDepartment = ClientDepartment::where('is_delete',0)->get();

        return view('admin.company.add_company_user',compact('getCompanyId','module','clientDepartment','uuid'));   
    }

    public function saveCompanyUser(Request $request){

        if($request->employee_id == ''){
            $saveUser = new Employee;
            $saveUser->name = $request->name;
            $saveUser->mobile_no = $request->mobile_number;
            $saveUser->email = $request->email;
            $saveUser->password = bcrypt($request->password);
            $saveUser->save();
        }

        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $emp = new EmployeeModule;
                if($request->employee_id != ''){
                    $emp->employee_id = $request->employee_id;
                } else {
                    $emp->employee_id = $saveUser->id;
                } 
                $emp->module_id = $mv;
                $emp->save();
            }
        }

        $addUser = new CompanyEmployee;
        $addUser->company_id = $request->company_id;
        $addUser->type = $request->user_type;
        $addUser->department_id = $request->department ? $request->department : null;
        if($request->employee_id != ''){
            $addUser->employee_id = $request->employee_id;
        } else {
            $addUser->employee_id = $saveUser->id;
        } 
        $addUser->save();

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addCompanyUser',$request->uuid))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Company',
                      'message' => 'Company User Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.companyTeamList',$request->uuid))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Company',
                      'message' => 'Company User Successfully Added',
                  ],
            ]); 
        }
    }

    public function editCompanyUser($uuid,$id){

        $module = CompanyModule::all();

        $getCompanyId = $this->getCompanyId($uuid);

        $employeeModule = array();

        $clientDepartment = ClientDepartment::where('is_delete',0)->get();

        $findCompanyUser = Employee::where('id',$id)->with(['employeemodule'])->first();

        $companyEmployeeUser = CompanyEmployee::where('company_id',$getCompanyId)->where('employee_id',$id)->first();

        if(!is_null($findCompanyUser)){
            foreach($findCompanyUser->employeemodule as $ek => $ev){
                $employeeModule[] = $ev->module_id;
            }
        }

        return view('admin.company.edit_company_user',compact('findCompanyUser','uuid','employeeModule','module','getCompanyId','clientDepartment','companyEmployeeUser'));   
    }

    public function saveEditedCompanyUser(Request $request){
        
        $deleteModule = EmployeeModule::where('employee_id',$request->employee_id)->delete();

        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $emp = new EmployeeModule;
                $emp->employee_id = $request->employee_id;
                $emp->module_id = $mv;
                $emp->save();
            }
        }

        $addUser = CompanyEmployee::findOrFail($request->company_employee_id);
        $addUser->type = $request->user_type;
        $addUser->department_id = $request->department ? $request->department : null;
        $addUser->save();

        return redirect(route('admin.companyTeamList',$request->uuid))->with('messages', [
            [
                'type' => 'success',
                'title' => 'Company',
                'message' => 'Company User Successfully Updated'
            ],
        ]); 
    }

    public function deleteCompanyUser($uuid,$id){

        $getCompanyId = $this->getCompanyId($uuid);

        $deleteCompanyUser = CompanyEmployee::where('company_id',$getCompanyId)->where('employee_id',$id)->delete();

        return redirect()->back()->with('messages', [
            [
                'type' => 'success',
                'title' => 'Company',
                'message' => 'Company User Successfully Deleted'
            ],
        ]); 
    }

    public function companyEmployeeList(){

        $getEmployeeList = Employee::where('is_delete',0)->get();

        return view('admin.company.employee.employee_list',compact('getEmployeeList'));
    }

    public function editCompanyEmployee($id){

        $getEmployee = Employee::where('id',$id)->first();

        return view('admin.company.employee.edit_employee',compact('getEmployee'));
    }

    public function saveEditedCompanyEmployee(Request $request){

        $emp = Employee::findOrFail($request->id);
        $emp->name = $request->name;
        $emp->email = $request->email;
        $emp->mobile_no = $request->mobile_number;
        if($request->password != ''){
            $emp->password = bcrypt($request->password);
        }
        $emp->save();

        return redirect(route('admin.companyEmployeeList'))->with('messages', [
            [
                'type' => 'success',
                'title' => 'Employee',
                'message' => 'Employee Successfully Updated'
            ],
        ]); 
    }

    public function deleteCompanyEmployee($id){

        $deleteCompany = Employee::where('id',$id)->update(['is_delete' => 1]);

        return redirect()->back()->with('messages', [
            [
                'type' => 'success',
                'title' => 'Employee',
                'message' => 'Employee Successfully Deleted'
            ],
        ]); 
    }

    public function checkEmployeeEmail(Request $request){

        $query = Employee::query();
        $query->where('email',$request->email);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $employee = $query->first();

        if(!is_null($employee)){
            return 'false';
        } else {
            return 'true';
        }
    }

    public function getEmployeeSuggestion(Request $request){
        
        $getCompanyEmployee = CompanyEmployee::where('company_id',$request->company_id)->pluck('employee_id');

        $getEmployeeSuggestion = Employee::whereNotIn('id',$getCompanyEmployee)->where('is_delete',0)->get();

        $employeeJson = array();

        if(!is_null($getEmployeeSuggestion)){
            foreach($getEmployeeSuggestion as $pk => $pv){
                $employeeJson[$pk]['label'] = $pv->name."(".$pv->email.")";
                $employeeJson[$pk]['value'] = $pv->id;
            }
        }

        return $employeeJson;    
    }

    public function employeeDetails(Request $request){

        $employee = Employee::where('id',$request->id)->first();

        $data['id'] = $employee->id;
        $data['name'] = $employee->name;
        $data['mobile'] = $employee->mobile_no;
        $data['email'] = $employee->email;

        return $data;

    }
}
