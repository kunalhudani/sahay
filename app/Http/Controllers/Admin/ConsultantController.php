<?php

namespace App\Http\Controllers\Admin;

use App\Model\City;
use App\Model\Module;
use App\Model\Consultant;
use Illuminate\Http\Request;
use App\Model\ConsultantCity;
use App\Model\ConsultantModule;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;

class ConsultantController extends GlobalController
{
    public function __construct(){
       $this->middleware('admin');
       $this->middleware('checkpermission');
    }

    //consultant listing
    public function consultantsList(){

        $get_consultants = Consultant::where('is_delete',0)->get();

        return view('admin.consultant.consultant_list',compact('get_consultants'));
    }

    //add consultant
    public function addConsultants(){

    	$get_city = City::get();
    	$getModule = Module::all();

        return view('admin.consultant.add_consultant',compact('get_city','getModule'));
    }

    public function saveConsultants(Request $request){
    	
        $uuid = $this->generateUUID();

        $saveConsultant = new Consultant;
        $saveConsultant->uuid = $uuid;
        $saveConsultant->name = $request->full_name;
        if($request->dob != ''){
            $saveConsultant->dob = $this->convertDate($request->dob);
        }
        $saveConsultant->address = $request->address;
        $saveConsultant->mobile = $request->mobile_number;
        $saveConsultant->status = $request->status;
        $saveConsultant->professional_email_id = $request->professional_email_id;
        $saveConsultant->personal_email_id = $request->personal_email_id;
        $saveConsultant->password = bcrypt($request->password);
        $saveConsultant->aadhar_card_number = $request->aadharcard_nunber;
        $saveConsultant->pan_number = strtoupper($request->pancard_number);
        $saveConsultant->gstn_number = strtoupper($request->gstn_number);
        $this->uploadProfile($uuid,$request->profile_image);
        $saveConsultant->profile_photo = $request->profile_image;
        if(isset($request->aadharcard_image)){
            $aadharcard_image = $this->uploadBucket($request->aadharcard_image,'aadharcard',$uuid);
            $saveConsultant->aadhar_card_image = $aadharcard_image;
        }
        if(isset($request->aadharcard_back_image)){
            $aadharcard_back_image = $this->uploadBucket($request->aadharcard_back_image,'aadharcard',$uuid);
            $saveConsultant->aadhar_card_back_image = $aadharcard_back_image;
        }
        if(isset($request->pancard_image)){
            $pancard_image = $this->uploadBucket($request->pancard_image,'pancard_image',$uuid);
            $saveConsultant->pan_card_image = $pancard_image;
        }
        if(isset($request->resume_upload)){
            $resume_upload = $this->uploadBucket($request->resume_upload,'resume',$uuid);
            $saveConsultant->resume_upload = $resume_upload;
        }
        if(isset($request->mou_upload)){
            $mou_upload = $this->uploadBucket($request->mou_upload,'mou_upload',$uuid);
            $saveConsultant->mou_upload = $mou_upload;
        }
        $saveConsultant->save();

        //city store
        if(!is_null($request->location)){
            foreach($request->location as $lk => $lv){
            	$location = new ConsultantCity;
                $location->consultant_id = $saveConsultant->id;
                $location->location_id = $lv;
                $location->save();
            }
        }

        //consultant module store
        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $module = new ConsultantModule;
                $module->consultant_id = $saveConsultant->id;
                $module->module_id = $mv;
                $module->save();
            }
        }

        if($request->btn_submit == 'save_and_update'){

            return redirect(route('admin.addConsultants'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Consultant',
                      'message' => 'Consultant Successfully Added',
                  ],
            ]); 

        } else {

            return redirect(route('admin.consultantsList'))->with('messages', [
                  [
                      'type' => 'success',
                      'title' => 'Consultant',
                      'message' => 'Consultant Successfully Added',
                  ],
            ]); 
        }
    }

    //edit consultant
    public function editConsultants($id){

        $get_city = City::get();
    	$getModule = Module::all();
        $find_consultant= Consultant::where('id',$id)->first();
        $find_consultant_location = ConsultantCity::where('consultant_id',$id)->pluck('location_id')->toArray();
        $getModuleId = ConsultantModule::where('consultant_id',$id)->pluck('module_id')->toArray();

        return view('admin.consultant.edit_consultant',compact('find_consultant','getModuleId','find_consultant_location','get_city','getModule'));
    }

    //upadate consultant
    public function saveEditedConsultants(Request $request){
    	
        $saveConsultant = Consultant::findOrFail($request->id);
        $saveConsultant->name = $request->full_name;
        if($request->dob != ''){
            $saveConsultant->dob = $this->convertDate($request->dob);
        } else {
            $saveConsultant->dob = null;
        }
        $saveConsultant->address = $request->address;
        $saveConsultant->mobile = $request->mobile_number;
        $saveConsultant->status = $request->status;
        $saveConsultant->professional_email_id = $request->professional_email_id;
        $saveConsultant->personal_email_id = $request->personal_email_id;
        if($request->password != ''){
            $saveConsultant->password = bcrypt($request->password);
        }
        $saveConsultant->aadhar_card_number = $request->aadharcard_nunber;
        $saveConsultant->pan_number = strtoupper($request->pancard_number);
        $saveConsultant->gstn_number = strtoupper($request->gstn_number);
        if($request->profile_image != ''){
        	$this->uploadProfile($request->uuid,$request->profile_image);
        	$saveConsultant->profile_photo = $request->profile_image;
        }
        if(isset($request->aadharcard_image)){
            $aadharcard_image = $this->uploadBucket($request->aadharcard_image,'aadharcard',$request->uuid);
            $saveConsultant->aadhar_card_image = $aadharcard_image;
        }
        if(isset($request->aadharcard_back_image)){
            $aadharcard_back_image = $this->uploadBucket($request->aadharcard_back_image,'aadharcard',$request->uuid);
            $saveConsultant->aadhar_card_back_image = $aadharcard_back_image;
        }
        if(isset($request->pancard_image)){
            $pancard_image = $this->uploadBucket($request->pancard_image,'pancard_image',$request->uuid);
            $saveConsultant->pan_card_image = $pancard_image;
        }
        if(isset($request->resume_upload)){
            $resume_upload = $this->uploadBucket($request->resume_upload,'resume',$request->uuid);
            $saveConsultant->resume_upload = $resume_upload;
        }
        if(isset($request->mou_upload)){
            $mou_upload = $this->uploadBucket($request->mou_upload,'mou_upload',$request->uuid);
            $saveConsultant->mou_upload = $mou_upload;
        }
        $saveConsultant->save();

        //city store
        $removeLocation = ConsultantCity::where('consultant_id',$request->id)->delete();
        if(!is_null($request->location)){
            foreach($request->location as $lk => $lv){
            	$location = new ConsultantCity;
                $location->consultant_id = $saveConsultant->id;
                $location->location_id = $lv;
                $location->save();
            }
        }

        //upadate consultant modules
        $removeModule = ConsultantModule::where('consultant_id',$request->id)->delete();
        if(!is_null($request->module)){
            foreach($request->module as $mk => $mv){
                $module = new ConsultantModule;
                $module->consultant_id = $request->id;
                $module->module_id = $mv;
                $module->save();
            }
        }

        return redirect(route('admin.consultantsList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Consultant',
                  'message' => 'Consultant Successfully Updated',
              ],
        ]); 
    }

    //remove consultant
    public function deleteConsultants($id){

        $delete_industry = Consultant::where('id',$id)->update(['is_delete' => 1]);

        return redirect(route('admin.consultantsList'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Consultant',
                  'message' => 'Consultant Successfully Deleted',
              ],
        ]); 
    }

    //check professional email exists or not
    public function checkProfessionalEmailExists(Request $request){

        $query = Consultant::query();
        $query->where('professional_email_id',$request->professional_email_id);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $professional_email_id = $query->first();

        if(!is_null($professional_email_id)){
            return 'false';
        } else {
            return 'true';
        }
    }

    //check personal email exists or not
    public function checkPersonalEmailExists(Request $request){

        $query = Consultant::query();
        $query->where('personal_email_id',$request->personal_email_id);
        if (isset($request->id)) {
            $query->where('id','!=',$request->id);
        }
        $personal_email_id = $query->first();

        if(!is_null($personal_email_id)){
            return 'false';
        } else {
            return 'true';
        }
    }
    
}
