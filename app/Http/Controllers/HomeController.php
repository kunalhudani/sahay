<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(){

        $this->middleware('auth',['except' => ['index','cropImage']]);
    }

    public function index(){

        return view('front.home.index');
    }

    public function clientDashboard(){

    	return view('client.dashboard.dashboard');	
    }

    //image crop function
    public function cropImage(Request $request){

        $image = $request->image;  // your base64 encoded
        $data = explode(';', $image);
        if($data[0] == 'data:image/jpeg'){
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $imageName = time().'.'.'jpg';
        } else {
            $image = str_replace('data:image/png;base64,', '', $image);
            $imageName = time().'.'.'jpg';
        }
        $image = str_replace(' ', '+', $image);
        \File::put(public_path(). '/uploads/' . $imageName, base64_decode($image));

        return response()->json(['success'=>'done','file' => $imageName]);
    }
}
