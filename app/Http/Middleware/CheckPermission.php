<?php

namespace App\Http\Middleware;

use App\Model\AdminModule;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $checkPermission = AdminModule::where('admin_id',Auth::guard('admin')->user()->id)->with(['module'])->get();
               
        $module = array();

        if(!is_null($checkPermission)){
            foreach($checkPermission as $pk => $pv){
                $module[] = $pv->module->slug;
            }
        }

        if(in_array(request()->segment(2),$module)){
            return $next($request);
        } else {
            abort('403');
        }
    }
}
